import type {
  OpenAPIClient,
  Parameters,
  UnknownParamsObject,
  OperationResponse,
  AxiosRequestConfig,
} from 'openapi-client-axios';

export declare namespace Components {
    namespace RequestBodies {
        export interface BlogPostCreateRequest {
            /**
             * ID of the space
             */
            spaceId: string;
            /**
             * The status of the blog post, specifies if the blog post will be created as a new blog post or a draft
             */
            status?: "current" | "draft";
            /**
             * Title of the blog post, required if creating non-draft.
             */
            title?: string;
            body?: Schemas.BlogPostBodyWrite | /**
             * Body of the blog post. Only one body format should be specified as the property
             * for this object, e.g. `storage`.
             */
            Schemas.BlogPostNestedBodyWrite;
        }
        export interface BlogPostUpdateRequest {
            /**
             * Id of the blog post.
             */
            id: string;
            /**
             * The updated status of the blog post. Note, if you change the status of a blog post from 'current' to 'draft' and it has an existing draft, the existing draft will be deleted in favor of the updated draft.
             */
            status: "current" | "draft";
            /**
             * Title of the blog post.
             */
            title: string;
            /**
             * ID of the containing space.
             *
             * This currently **does not support moving the blog post to a different space**.
             */
            spaceId?: string;
            body: Schemas.BlogPostBodyWrite | /**
             * Body of the blog post. Only one body format should be specified as the property
             * for this object, e.g. `storage`.
             */
            Schemas.BlogPostNestedBodyWrite;
            version: {
                /**
                 * The new version number of the updated blog post.
                 * Set this to the current version number plus one, unless you are updating the status to 'draft' which requires a version number of 1.
                 *
                 * If you don't know the current version number, use Get blog post by id.
                 */
                number?: number; // int32
                /**
                 * An optional message to be stored with the version.
                 */
                message?: string;
            };
        }
        export interface CheckAccessOrInviteByEmailRequest {
            /**
             * List of emails to check access to site.
             */
            emails: [
                string,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?
            ];
        }
        export interface ContentIdToContentTypeRequest {
            /**
             * The content ids to convert. They may be provided as strings or numbers.
             */
            contentIds: [
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?,
                (string | number)?
            ];
        }
        export interface CustomContentCreateRequest {
            /**
             * Type of custom content.
             */
            type: string;
            /**
             * The status of the custom content
             */
            status?: "current";
            /**
             * ID of the containing space
             */
            spaceId?: string;
            /**
             * ID of the containing page
             */
            pageId?: string;
            /**
             * ID of the containing Blog Post
             */
            blogPostId?: string;
            /**
             * ID of the containing custom content
             */
            customContentId?: string;
            /**
             * Title of the custom content
             */
            title: string;
            body: Schemas.CustomContentBodyWrite | /**
             * Body of the custom content. Only one body format should be specified as the property
             * for this object, e.g. `storage`.
             */
            Schemas.CustomContentNestedBodyWrite;
        }
        export interface CustomContentUpdateRequest {
            /**
             * Id of custom content.
             */
            id: string;
            /**
             * Type of custom content.
             */
            type: string;
            /**
             * The status of the custom content
             */
            status: "current";
            /**
             * ID of the containing space
             */
            spaceId?: string;
            /**
             * ID of the containing page
             */
            pageId?: string;
            /**
             * ID of the containing Blog Post
             */
            blogPostId?: string;
            /**
             * ID of the containing custom content
             */
            customContentId?: string;
            /**
             * Title of the custom content
             */
            title: string;
            body: Schemas.CustomContentBodyWrite | /**
             * Body of the custom content. Only one body format should be specified as the property
             * for this object, e.g. `storage`.
             */
            Schemas.CustomContentNestedBodyWrite;
            version: {
                /**
                 * The version number, must be incremented by one.
                 */
                number?: number; // int32
                /**
                 * An optional message to be stored with the version.
                 */
                message?: string;
            };
        }
        export interface PageCreateRequest {
            /**
             * ID of the space
             */
            spaceId: string;
            /**
             * The status of the page, published or draft.
             */
            status?: "current" | "draft";
            /**
             * Title of the page, required if page status is not draft.
             */
            title?: string;
            /**
             * The parent content ID of the page.
             */
            parentId?: string;
            body?: Schemas.PageBodyWrite | /**
             * Body of the page. Only one body format should be specified as the property
             * for this object, e.g. `storage`.
             */
            Schemas.PageNestedBodyWrite;
        }
        export interface PageUpdateRequest {
            /**
             * Id of the page.
             */
            id: string;
            /**
             * The updated status of the page. Note, if you change the status of a page from 'current' to 'draft' and it has an existing draft, the existing draft will be deleted in favor of the updated draft.
             */
            status: "current" | "draft";
            /**
             * Title of the page.
             */
            title: string;
            /**
             * ID of the containing space.
             *
             * This currently **does not support moving the page to a different space**.
             */
            spaceId?: any; // string
            /**
             * ID of the parent page.
             *
             * This allows the page to be moved under a different parent within the same space.
             */
            parentId?: any; // string
            /**
             * Account ID of the page owner.
             *
             * This allows page ownership to be transferred to another user.
             */
            ownerId?: any; // string
            body: Schemas.PageBodyWrite | /**
             * Body of the page. Only one body format should be specified as the property
             * for this object, e.g. `storage`.
             */
            Schemas.PageNestedBodyWrite;
            version: {
                /**
                 * The new version of the updated page.
                 * Set this to the current version number plus one, unless you are updating the status to 'draft' which requires a version number of 1.
                 *
                 * If you don't know the current version number, use Get page by id.
                 */
                number?: number; // int32
                /**
                 * An optional message to be stored with the version.
                 */
                message?: string;
            };
        }
        export interface TaskUpdateRequest {
            /**
             * ID of the task.
             */
            id?: string;
            /**
             * Local ID of the task. This ID is local to the corresponding page or blog post.
             */
            localId?: string;
            /**
             * ID of the space the task is in.
             */
            spaceId?: string;
            /**
             * ID of the page the task is in.
             */
            pageId?: string;
            /**
             * ID of the blog post the task is in.
             */
            blogPostId?: string;
            /**
             * Status of the task.
             */
            status: "complete" | "incomplete";
            /**
             * Account ID of the user who created this task.
             */
            createdBy?: string;
            /**
             * Account ID of the user to whom this task is assigned.
             */
            assignedTo?: string;
            /**
             * Account ID of the user who completed this task.
             */
            completedBy?: string;
            /**
             * Date and time when the task was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            /**
             * Date and time when the task was updated. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            updatedAt?: string; // date-time
            /**
             * Date and time when the task is due. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            dueAt?: string; // date-time
            /**
             * Date and time when the task was completed. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            completedAt?: string; // date-time
        }
        export interface WhiteboardCreateRequest {
            /**
             * ID of the space
             */
            spaceId: string;
            /**
             * Title of the whiteboard
             */
            title?: string;
            /**
             * The parent content ID of the whiteboard
             */
            parentId?: string;
        }
    }
    namespace Schemas {
        export interface AbstractPageLinks {
            /**
             * Web UI link of the content.
             */
            webui?: string;
            /**
             * Edit UI link of the content.
             */
            editui?: string;
            /**
             * Web UI link of the content.
             */
            tinyui?: string;
        }
        export interface Ancestor {
            /**
             * ID of the ancestor
             */
            id?: string;
            type?: /* The type of ancestor. */ AncestorType;
        }
        /**
         * The type of ancestor.
         */
        export type AncestorType = "page" | "whiteboard";
        export interface Attachment {
            /**
             * ID of the attachment.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the comment.
             */
            title?: string;
            /**
             * Date and time when the attachment was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            /**
             * ID of the containing page.
             *
             * Note: This is only returned if the attachment has a container that is a page.
             */
            pageId?: string;
            /**
             * ID of the containing blog post.
             *
             * Note: This is only returned if the attachment has a container that is a blog post.
             */
            blogPostId?: string;
            /**
             * ID of the containing custom content.
             *
             * Note: This is only returned if the attachment has a container that is custom content.
             */
            customContentId?: string;
            /**
             * Media Type for the attachment.
             */
            mediaType?: string;
            /**
             * Media Type description for the attachment.
             */
            mediaTypeDescription?: string;
            /**
             * Comment for the attachment.
             */
            comment?: string;
            /**
             * File ID of the attachment. This is the ID referenced in `atlas_doc_format` bodies and is distinct from the attachment ID.
             */
            fileId?: string;
            /**
             * File size of the attachment.
             */
            fileSize?: number; // int64
            /**
             * WebUI link of the attachment.
             */
            webuiLink?: string;
            /**
             * Download link of the attachment.
             */
            downloadLink?: string;
            version?: Version;
            _links?: AttachmentLinks;
        }
        export interface AttachmentCommentModel {
            /**
             * ID of the comment.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the comment.
             */
            title?: string;
            /**
             * ID of the attachment containing the comment.
             */
            attachmentId?: string;
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodySingle;
            _links?: CommentLinks;
        }
        export interface AttachmentLinks {
            /**
             * Web UI link of the content.
             */
            webui?: string;
            /**
             * Download link of the content.
             */
            download?: string;
        }
        /**
         * The sort fields for attachments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`.
         */
        export type AttachmentSortOrder = "created-date" | "-created-date" | "modified-date" | "-modified-date";
        export interface AttachmentVersion {
            /**
             * Date and time when the version was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            /**
             * Message associated with the current version.
             */
            message?: string;
            /**
             * The version number.
             */
            number?: number; // int32
            /**
             * Describes if this version is a minor version. Email notifications and activity stream updates are not created for minor versions.
             */
            minorEdit?: boolean;
            /**
             * The account ID of the user who created this version.
             */
            authorId?: string;
            attachment?: VersionedEntity;
        }
        export interface BlogPostBodyWrite {
            /**
             * Type of content representation used for the value field.
             */
            representation?: "storage" | "atlas_doc_format" | "wiki";
            /**
             * Body of the blog post, in the format found in the representation field.
             */
            value?: string;
        }
        export interface BlogPostBulk {
            /**
             * ID of the blog post.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the blog post.
             */
            title?: string;
            /**
             * ID of the space the blog post is in.
             */
            spaceId?: string;
            /**
             * The account ID of the user who created this blog post originally.
             */
            authorId?: string;
            /**
             * Date and time when the blog post was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodyBulk;
            _links?: AbstractPageLinks;
        }
        export interface BlogPostCommentModel {
            /**
             * ID of the comment.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the comment.
             */
            title?: string;
            /**
             * ID of the blog post the comment is in.
             */
            blogPostId?: string;
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodyBulk;
            _links?: CommentLinks;
        }
        export interface BlogPostInlineCommentModel {
            /**
             * ID of the comment.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the comment.
             */
            title?: string;
            /**
             * ID of the blog post the comment is in.
             */
            blogPostId?: string;
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodyBulk;
            resolutionStatus?: /* Inline comment resolution status */ InlineCommentResolutionStatus;
            properties?: InlineCommentProperties;
            _links?: CommentLinks;
        }
        /**
         * Body of the blog post. Only one body format should be specified as the property
         * for this object, e.g. `storage`.
         */
        export interface BlogPostNestedBodyWrite {
            storage?: BlogPostBodyWrite;
            atlas_doc_format?: BlogPostBodyWrite;
            wiki?: BlogPostBodyWrite;
        }
        export interface BlogPostSingle {
            /**
             * ID of the blog post.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the blog post.
             */
            title?: string;
            /**
             * ID of the space the blog post is in.
             */
            spaceId?: string;
            /**
             * The account ID of the user who created this blog post originally.
             */
            authorId?: string;
            /**
             * Date and time when the blog post was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodySingle;
            _links?: AbstractPageLinks;
        }
        /**
         * The sort fields for blog posts. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`.
         */
        export type BlogPostSortOrder = "id" | "-id" | "created-date" | "-created-date" | "modified-date" | "-modified-date";
        export interface BlogPostVersion {
            /**
             * Date and time when the version was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            /**
             * Message associated with the current version.
             */
            message?: string;
            /**
             * The version number.
             */
            number?: number; // int32
            /**
             * Describes if this version is a minor version. Email notifications and activity stream updates are not created for minor versions.
             */
            minorEdit?: boolean;
            /**
             * The account ID of the user who created this version.
             */
            authorId?: string;
            blogpost?: VersionedEntity;
        }
        /**
         * Contains fields for each representation type requested.
         */
        export interface BodyBulk {
            storage?: BodyType;
            atlas_doc_format?: BodyType;
        }
        /**
         * Contains fields for each representation type requested.
         */
        export interface BodySingle {
            storage?: BodyType;
            atlas_doc_format?: BodyType;
            view?: BodyType;
        }
        export interface BodyType {
            /**
             * Type of content representation used for the value field.
             */
            representation?: string;
            /**
             * Body of the content, in the format found in the representation field.
             */
            value?: string;
        }
        export interface ChildCustomContent {
            /**
             * ID of the child custom content.
             */
            id?: string;
            status?: /* The status of the content. */ OnlyArchivedAndCurrentContentStatus;
            /**
             * Title of the custom content.
             */
            title?: string;
            /**
             * Custom content type.
             */
            type?: string;
            /**
             * ID of the space the custom content is in.
             */
            spaceId?: string;
        }
        /**
         * The sort fields for child custom content. The default sort direction is ascending by id. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`.
         */
        export type ChildCustomContentSortOrder = "created-date" | "-created-date" | "id" | "-id" | "modified-date" | "-modified-date";
        export interface ChildPage {
            /**
             * ID of the page.
             */
            id?: string;
            status?: /* The status of the content. */ OnlyArchivedAndCurrentContentStatus;
            /**
             * Title of the page.
             */
            title?: string;
            /**
             * ID of the space the page is in.
             */
            spaceId?: string;
            /**
             * Position of child page within the given parent page tree.
             */
            childPosition?: number | null; // int32
        }
        /**
         * The sort fields for child pages. The default sort direction is ascending by child-position. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`.
         */
        export type ChildPageSortOrder = "created-date" | "-created-date" | "id" | "-id" | "child-position" | "-child-position" | "modified-date" | "-modified-date";
        export interface ChildrenCommentModel {
            /**
             * ID of the comment.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the comment.
             */
            title?: string;
            /**
             * ID of the parent comment the child comment is in.
             */
            parentCommentId?: string;
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodyBulk;
            _links?: CommentLinks;
        }
        export interface CommentBodyWrite {
            /**
             * Type of content representation used for the value field.
             */
            representation?: "storage" | "atlas_doc_format" | "wiki";
            /**
             * Body of the comment, in the format found in the representation field.
             */
            value?: string;
        }
        export interface CommentLinks {
            /**
             * Web UI link of the content.
             */
            webui?: string;
        }
        /**
         * Body of the comment. Only one body format should be specified as the property
         * for this object, e.g. `storage`.
         */
        export interface CommentNestedBodyWrite {
            storage?: CommentBodyWrite;
            atlas_doc_format?: CommentBodyWrite;
            wiki?: CommentBodyWrite;
        }
        /**
         * The sort fields for comments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`.
         */
        export type CommentSortOrder = "created-date" | "-created-date" | "modified-date" | "-modified-date";
        export interface CommentVersion {
            /**
             * Date and time when the version was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            /**
             * Message associated with the current version.
             */
            message?: string;
            /**
             * The version number.
             */
            number?: number; // int32
            /**
             * Describes if this version is a minor version. Email notifications and activity stream updates are not created for minor versions.
             */
            minorEdit?: boolean;
            /**
             * The account ID of the user who created this version.
             */
            authorId?: string;
            comment?: VersionedEntity;
        }
        export interface ContentIdToContentTypeResponse {
            /**
             * JSON object containing all requested content ids as keys and their associated content types as the values.
             * Duplicate content ids in the request will be returned under a single key in the response. For built-in content
             * types, the enumerations are as specified. Custom content ids will be mapped to their associated type.
             */
            results?: {
                [name: string]: ("page" | "blogpost" | "attachment" | "footer-comment" | "inline-comment") | string;
            };
        }
        export interface ContentProperty {
            /**
             * ID of the property
             */
            id?: string;
            /**
             * Key of the property
             */
            key?: string;
            /**
             * Value of the property. Must be a valid JSON value.
             */
            value?: any;
            version?: Version;
        }
        export interface ContentPropertyCreateRequest {
            /**
             * Key of the content property
             */
            key?: string;
            /**
             * Value of the content property.
             */
            value?: any;
        }
        /**
         * The sort fields for content properties. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`.
         */
        export type ContentPropertySortOrder = "key" | "-key";
        export interface ContentPropertyUpdateRequest {
            /**
             * Key of the content property
             */
            key?: string;
            /**
             * Value of the content property.
             */
            value?: any;
            /**
             * New version number and associated message
             */
            version?: {
                /**
                 * Version number of the new version. Should be 1 more than the current version number.
                 */
                number?: number; // int32
                /**
                 * Message to be associated with the new version.
                 */
                message?: string;
            };
        }
        /**
         * The status of the content.
         */
        export type ContentStatus = "current" | "trashed" | "historical" | "deleted" | "any" | "draft" | "archived";
        export interface CreateFooterCommentModel {
            /**
             * ID of the containing blog post, if intending to create a top level footer comment. Do not provide if creating a reply.
             */
            blogPostId?: string;
            /**
             * ID of the containing page, if intending to create a top level footer comment. Do not provide if creating a reply.
             */
            pageId?: string;
            /**
             * ID of the parent comment, if intending to create a reply. Do not provide if creating a top level comment.
             */
            parentCommentId?: string;
            /**
             * ID of the attachment, if intending to create a comment against an attachment.
             */
            attachmentId?: string;
            /**
             * ID of the custom content, if intending to create a comment against a custom content.
             */
            customContentId?: string;
            body?: CommentBodyWrite | /**
             * Body of the comment. Only one body format should be specified as the property
             * for this object, e.g. `storage`.
             */
            CommentNestedBodyWrite;
        }
        export interface CreateInlineCommentModel {
            /**
             * ID of the containing blog post, if intending to create a top level footer comment. Do not provide if creating a reply.
             */
            blogPostId?: string;
            /**
             * ID of the containing page, if intending to create a top level footer comment. Do not provide if creating a reply.
             */
            pageId?: string;
            /**
             * ID of the parent comment, if intending to create a reply. Do not provide if creating a top level comment.
             */
            parentCommentId?: string;
            body?: CommentBodyWrite | /**
             * Body of the comment. Only one body format should be specified as the property
             * for this object, e.g. `storage`.
             */
            CommentNestedBodyWrite;
            /**
             * Object describing the text to highlight on the page/blog post. Only applicable for top level inline comments (not replies) and required in that case.
             */
            inlineCommentProperties?: {
                /**
                 * The text to highlight
                 */
                textSelection?: string;
                /**
                 * The number of matches for the selected text on the page (should be strictly greater than textSelectionMatchIndex)
                 */
                textSelectionMatchCount?: number;
                /**
                 * The match index to highlight. This is zero-based. E.g. if you have 3 occurrences of "hello world" on a page
                 * and you want to highlight the second occurrence, you should pass 1 for textSelectionMatchIndex and 3 for textSelectionMatchCount.
                 */
                textSelectionMatchIndex?: number;
            };
        }
        /**
         * Contains fields for each representation type requested.
         */
        export interface CustomContentBodyBulk {
            raw?: BodyType;
            storage?: BodyType;
            atlas_doc_format?: BodyType;
        }
        /**
         * The formats a custom content body can be represented as. A subset of BodyRepresentation.
         */
        export type CustomContentBodyRepresentation = "raw" | "storage" | "atlas_doc_format";
        /**
         * The formats a custom content body can be represented as. A subset of BodyRepresentation.
         */
        export type CustomContentBodyRepresentationSingle = "raw" | "storage" | "atlas_doc_format" | "view" | "export_view" | "anonymous_export_view";
        /**
         * Contains fields for each representation type requested.
         */
        export interface CustomContentBodySingle {
            raw?: BodyType;
            storage?: BodyType;
            atlas_doc_format?: BodyType;
            view?: BodyType;
        }
        export interface CustomContentBodyWrite {
            /**
             * Type of content representation used for the value field.
             */
            representation?: "storage" | "atlas_doc_format" | "raw";
            /**
             * Body of the custom content, in the format found in the representation field.
             */
            value?: string;
        }
        export interface CustomContentBulk {
            /**
             * ID of the custom content.
             */
            id?: string;
            /**
             * The type of custom content.
             */
            type?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the custom content.
             */
            title?: string;
            /**
             * ID of the space the custom content is in.
             *
             * Note: This is always returned, regardless of if the custom content has a container that is a space.
             */
            spaceId?: string;
            /**
             * ID of the containing page.
             *
             * Note: This is only returned if the custom content has a container that is a page.
             */
            pageId?: string;
            /**
             * ID of the containing blog post.
             *
             * Note: This is only returned if the custom content has a container that is a blog post.
             */
            blogPostId?: string;
            /**
             * ID of the containing custom content.
             *
             * Note: This is only returned if the custom content has a container that is custom content.
             */
            customContentId?: string;
            /**
             * The account ID of the user who created this custom content originally.
             */
            authorId?: string;
            /**
             * Date and time when the custom content was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ CustomContentBodyBulk;
            _links?: CustomContentLinks;
        }
        export interface CustomContentCommentModel {
            /**
             * ID of the comment.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the comment.
             */
            title?: string;
            /**
             * ID of the custom content containing the comment.
             */
            customContentId?: string;
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodySingle;
            _links?: CommentLinks;
        }
        export interface CustomContentLinks {
            /**
             * Web UI link of the content.
             */
            webui?: string;
        }
        /**
         * Body of the custom content. Only one body format should be specified as the property
         * for this object, e.g. `storage`.
         */
        export interface CustomContentNestedBodyWrite {
            storage?: CustomContentBodyWrite;
            atlas_doc_format?: CustomContentBodyWrite;
            raw?: CustomContentBodyWrite;
        }
        export interface CustomContentSingle {
            /**
             * ID of the custom content.
             */
            id?: string;
            /**
             * The type of custom content.
             */
            type?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the custom content.
             */
            title?: string;
            /**
             * ID of the space the custom content is in.
             *
             * Note: This is always returned, regardless of if the custom content has a container that is a space.
             */
            spaceId?: string;
            /**
             * ID of the containing page.
             *
             * Note: This is only returned if the custom content has a container that is a page.
             */
            pageId?: string;
            /**
             * ID of the containing blog post.
             *
             * Note: This is only returned if the custom content has a container that is a blog post.
             */
            blogPostId?: string;
            /**
             * ID of the containing custom content.
             *
             * Note: This is only returned if the custom content has a container that is custom content.
             */
            customContentId?: string;
            /**
             * The account ID of the user who created this custom content originally.
             */
            authorId?: string;
            /**
             * Date and time when the custom content was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ CustomContentBodySingle;
            _links?: CustomContentLinks;
        }
        /**
         * The sort fields for custom content. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`.
         */
        export type CustomContentSortOrder = "id" | "-id" | "created-date" | "-created-date" | "modified-date" | "-modified-date" | "title" | "-title";
        export interface CustomContentVersion {
            /**
             * Date and time when the version was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            /**
             * Message associated with the current version.
             */
            message?: string;
            /**
             * The version number.
             */
            number?: number; // int32
            /**
             * Describes if this version is a minor version. Email notifications and activity stream updates are not created for minor versions.
             */
            minorEdit?: boolean;
            /**
             * The account ID of the user who created this version.
             */
            authorId?: string;
            custom?: VersionedEntity;
        }
        /**
         * Details about data policies.
         */
        export interface DataPolicyMetadata {
            /**
             * Whether the workspace contains any content blocked for (inaccessible to) the requesting client application.
             */
            anyContentBlocked?: boolean;
        }
        export interface DataPolicySpace {
            /**
             * ID of the space.
             */
            id?: string;
            /**
             * Key of the space.
             */
            key?: string;
            /**
             * Name of the space.
             */
            name?: string;
            description?: /* Contains fields for each representation type requested. */ SpaceDescription;
            dataPolicy?: {
                /**
                 * Whether the space contains any content blocked for (inaccessible to) the requesting client application.
                 */
                anyContentBlocked?: boolean;
            };
            icon?: /* The icon of the space */ SpaceIcon;
            _links?: SpaceLinks;
        }
        export interface DetailedVersion {
            /**
             * The current version number.
             */
            number?: number; // int32
            /**
             * The account ID of the user who created this version.
             */
            authorId?: string;
            /**
             * Message associated with the current version.
             */
            message?: string;
            /**
             * Date and time when the version was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            /**
             * Describes if this version is a minor version. Email notifications and activity stream updates are not created for minor versions.
             */
            minorEdit?: boolean;
            /**
             * Describes if the content type is modified in this version (e.g. page to blog)
             */
            contentTypeModified?: boolean;
            /**
             * The account IDs of users that collaborated on this version.
             */
            collaborators?: string[];
            /**
             * The version number of the version prior to this current content update.
             */
            prevVersion?: number; // int32
            /**
             * The version number of the version after this current content update.
             */
            nextVersion?: number; // int32
        }
        export interface FooterCommentModel {
            /**
             * ID of the comment.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the comment.
             */
            title?: string;
            /**
             * ID of the blog post containing the comment if the comment is on a blog post.
             */
            blogPostId?: string;
            /**
             * ID of the page containing the comment if the comment is on a page.
             */
            pageId?: string;
            /**
             * ID of the attachment containing the comment if the comment is on an attachment.
             */
            attachmentId?: string;
            /**
             * ID of the custom content containing the comment if the comment is on a custom content.
             */
            customContentId?: string;
            /**
             * ID of the parent comment if the comment is a reply.
             */
            parentCommentId?: string;
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodySingle;
            _links?: CommentLinks;
        }
        export interface InlineCommentChildrenModel {
            /**
             * ID of the comment.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the comment.
             */
            title?: string;
            /**
             * ID of the parent comment the child comment is in.
             */
            parentCommentId?: string;
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodyBulk;
            resolutionStatus?: /* Inline comment resolution status */ InlineCommentResolutionStatus;
            properties?: InlineCommentProperties;
            _links?: CommentLinks;
        }
        export interface InlineCommentModel {
            /**
             * ID of the comment.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the comment.
             */
            title?: string;
            /**
             * ID of the blog post containing the comment if the comment is on a blog post.
             */
            blogPostId?: string;
            /**
             * ID of the page containing the comment if the comment is on a page.
             */
            pageId?: string;
            /**
             * ID of the parent comment if the comment is a reply.
             */
            parentCommentId?: string;
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodySingle;
            /**
             * Atlassian Account ID of last person who modified the resolve state of the comment. Null until comment is resolved or reopened.
             */
            resolutionLastModifierId?: string;
            /**
             * Timestamp of the last modification to the comment's resolution status. Null until comment is resolved or reopened.
             */
            resolutionLastModifiedAt?: string; // date-time
            resolutionStatus?: /* Inline comment resolution status */ InlineCommentResolutionStatus;
            properties?: InlineCommentProperties;
            _links?: CommentLinks;
        }
        export interface InlineCommentProperties {
            /**
             * Property value used to reference the highlighted element in DOM.
             */
            inlineMarkerRef?: string;
            /**
             * Text that is highlighted.
             */
            inlineOriginalSelection?: string;
            /**
             * Deprecated, use `inlineMarkerRef` instead.
             */
            "inline-marker-ref"?: string;
            /**
             * Deprecated, use `inlineOriginalSelection` instead.
             */
            "inline-original-selection"?: string;
        }
        /**
         * Inline comment resolution status
         */
        export type InlineCommentResolutionStatus = "open" | "reopened" | "resolved" | "dangling";
        export interface Label {
            /**
             * ID of the label.
             */
            id?: string;
            /**
             * Name of the label.
             */
            name?: string;
            /**
             * Prefix of the label.
             */
            prefix?: string;
        }
        /**
         * The sort fields for labels. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`.
         */
        export type LabelSortOrder = "created-date" | "-created-date" | "id" | "-id" | "name" | "-name";
        export interface Like {
            /**
             * Account ID.
             */
            accountId?: string;
        }
        /**
         * The status of the content.
         */
        export type OnlyArchivedAndCurrentContentStatus = "current" | "archived";
        export interface PageBodyWrite {
            /**
             * Type of content representation used for the value field.
             */
            representation?: "storage" | "atlas_doc_format" | "wiki";
            /**
             * Body of the page, in the format found in the representation field.
             */
            value?: string;
        }
        export interface PageBulk {
            /**
             * ID of the page.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the page.
             */
            title?: string;
            /**
             * ID of the space the page is in.
             */
            spaceId?: string;
            /**
             * ID of the parent page, or null if there is no parent page.
             */
            parentId?: string;
            parentType?: /* Content type of the parent, or null if there is no parent. */ ParentContentType;
            /**
             * Position of child page within the given parent page tree.
             */
            position?: number | null; // int32
            /**
             * The account ID of the user who created this page originally.
             */
            authorId?: string;
            /**
             * The account ID of the user who owns this page.
             */
            ownerId?: string;
            /**
             * The account ID of the user who owned this page previously, or null if there is no previous owner.
             */
            lastOwnerId?: string | null;
            /**
             * Date and time when the page was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodyBulk;
            _links?: AbstractPageLinks;
        }
        export interface PageCommentModel {
            /**
             * ID of the comment.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the comment.
             */
            title?: string;
            /**
             * ID of the page the comment is in.
             */
            pageId?: string;
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodyBulk;
            _links?: CommentLinks;
        }
        export interface PageInlineCommentModel {
            /**
             * ID of the comment.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the comment.
             */
            title?: string;
            /**
             * ID of the page the comment is in.
             */
            pageId?: string;
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodyBulk;
            resolutionStatus?: /* Inline comment resolution status */ InlineCommentResolutionStatus;
            properties?: InlineCommentProperties;
            _links?: CommentLinks;
        }
        /**
         * Body of the page. Only one body format should be specified as the property
         * for this object, e.g. `storage`.
         */
        export interface PageNestedBodyWrite {
            storage?: PageBodyWrite;
            atlas_doc_format?: PageBodyWrite;
            wiki?: PageBodyWrite;
        }
        export interface PageSingle {
            /**
             * ID of the page.
             */
            id?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the page.
             */
            title?: string;
            /**
             * ID of the space the page is in.
             */
            spaceId?: string;
            /**
             * ID of the parent page, or null if there is no parent page.
             */
            parentId?: string;
            parentType?: /* Content type of the parent, or null if there is no parent. */ ParentContentType;
            /**
             * Position of child page within the given parent page tree.
             */
            position?: number | null; // int32
            /**
             * The account ID of the user who created this page originally.
             */
            authorId?: string;
            /**
             * The account ID of the user who owns this page.
             */
            ownerId?: string;
            /**
             * The account ID of the user who owned this page previously, or null if there is no previous owner.
             */
            lastOwnerId?: string | null;
            /**
             * Date and time when the page was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            version?: Version;
            body?: /* Contains fields for each representation type requested. */ BodySingle;
            _links?: AbstractPageLinks;
        }
        /**
         * The sort fields for pages. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`.
         */
        export type PageSortOrder = "id" | "-id" | "created-date" | "-created-date" | "modified-date" | "-modified-date" | "title" | "-title";
        export interface PageVersion {
            /**
             * Date and time when the version was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            /**
             * Message associated with the current version.
             */
            message?: string;
            /**
             * The version number.
             */
            number?: number; // int32
            /**
             * Describes if this version is a minor version. Email notifications and activity stream updates are not created for minor versions.
             */
            minorEdit?: boolean;
            /**
             * The account ID of the user who created this version.
             */
            authorId?: string;
            page?: VersionedEntity;
        }
        /**
         * Content type of the parent, or null if there is no parent.
         */
        export type ParentContentType = "page" | "whiteboard";
        /**
         * The list of operations permitted on entity.
         */
        export interface PermittedOperationsResponse {
            operations?: {
                /**
                 * The type of operation.
                 */
                operation?: string;
                /**
                 * The type of entity the operation type targets.
                 */
                targetType?: string;
            }[];
        }
        /**
         * The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases.
         */
        export type PrimaryBodyRepresentation = "storage" | "atlas_doc_format";
        /**
         * The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases.
         */
        export type PrimaryBodyRepresentationSingle = "storage" | "atlas_doc_format" | "view" | "export_view" | "anonymous_export_view";
        export interface Space {
            /**
             * ID of the space.
             */
            id?: string;
            /**
             * Key of the space.
             */
            key?: string;
            /**
             * Name of the space.
             */
            name?: string;
            type?: /* The type of space. */ SpaceType;
            status?: /* The status of the space. */ SpaceStatus;
            /**
             * The account ID of the user who created this space originally.
             */
            authorId?: string;
            /**
             * Date and time when the space was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            /**
             * ID of the space's homepage.
             */
            homepageId?: string;
            description?: /* Contains fields for each representation type requested. */ SpaceDescription;
            icon?: /* The icon of the space */ SpaceIcon;
            _links?: SpaceLinks;
        }
        /**
         * Contains fields for each representation type requested.
         */
        export interface SpaceDescription {
            plain?: BodyType;
            view?: BodyType;
        }
        /**
         * The formats a space description can be represented as. A subset of BodyRepresentation.
         */
        export type SpaceDescriptionBodyRepresentation = "plain" | "view";
        /**
         * The icon of the space
         */
        export interface SpaceIcon {
            /**
             * The path (relative to base URL) at which the space's icon can be retrieved. The format should be like `/wiki/download/...` or `/wiki/aa-avatar/...`
             */
            path?: string;
            /**
             * The path (relative to base URL) that can be used to retrieve a link to download the space icon. 3LO apps should use this link instead of the value provided
             * in the `path` property to retrieve the icon.
             *
             * Currently this field is only returned for `global` spaces and not `personal` spaces.
             *
             */
            apiDownloadLink?: string;
        }
        export interface SpaceLinks {
            /**
             * Web UI link of the space.
             */
            webui?: string;
        }
        export interface SpacePermission {
            /**
             * ID of the space permission.
             */
            id?: string;
            /**
             * The entity the space permissions corresponds to.
             */
            principal?: {
                type?: "user" | "group" | "role";
                /**
                 * ID of the entity.
                 */
                id?: string;
            };
            /**
             * The operation the space permission corresponds to.
             */
            operation?: {
                /**
                 * The type of operation.
                 */
                key?: "use" | "create" | "read" | "update" | "delete" | "copy" | "move" | "export" | "purge" | "purge_version" | "administer" | "restore" | "create_space" | "restrict_content" | "archive";
                /**
                 * The type of entity the operation type targets.
                 */
                targetType?: "page" | "blogpost" | "comment" | "attachment" | "whiteboard" | "space" | "application" | "userProfile";
            };
        }
        export interface SpaceProperty {
            /**
             * ID of the space property.
             */
            id?: string;
            /**
             * Key of the space property.
             */
            key?: string;
            /**
             * Value of the space property.
             */
            value?: any;
            /**
             * RFC3339 compliant date time at which the property was created.
             */
            createdAt?: string; // date-time
            /**
             * Atlassian account ID of the user that created the space property.
             */
            createdBy?: string;
            version?: {
                /**
                 * RFC3339 compliant date time at which the property's current version was created.
                 */
                createdAt?: string; // date-time
                /**
                 * Atlassian account ID of the user that created the space property's current version.
                 */
                createdBy?: string;
                /**
                 * Message associated with the current version.
                 */
                message?: string;
                /**
                 * The space property's current version number.
                 */
                number?: number; // int32
            };
        }
        export interface SpacePropertyCreateRequest {
            /**
             * Key of the space property
             */
            key?: string;
            /**
             * Value of the space property.
             */
            value?: any;
        }
        export interface SpacePropertyUpdateRequest {
            /**
             * Key of the space property
             */
            key?: string;
            /**
             * Value of the space property.
             */
            value?: any;
            /**
             * New version number and associated message
             */
            version?: {
                /**
                 * Version number of the new version. Should be 1 more than the current version number.
                 */
                number?: number; // int32
                /**
                 * Message to be associated with the new version.
                 */
                message?: string;
            };
        }
        /**
         * The sort fields for spaces. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`.
         */
        export type SpaceSortOrder = "id" | "-id" | "key" | "-key" | "name" | "-name";
        /**
         * The status of the space.
         */
        export type SpaceStatus = "current" | "archived";
        /**
         * The type of space.
         */
        export type SpaceType = "global" | "personal";
        export interface Task {
            /**
             * ID of the task.
             */
            id?: string;
            /**
             * Local ID of the task. This ID is local to the corresponding page or blog post.
             */
            localId?: string;
            /**
             * ID of the space the task is in.
             */
            spaceId?: string;
            /**
             * ID of the page the task is in.
             */
            pageId?: string;
            /**
             * ID of the blog post the task is in.
             */
            blogPostId?: string;
            /**
             * Status of the task.
             */
            status?: "complete" | "incomplete";
            body?: /* Contains fields for each representation type requested. */ TaskBodySingle;
            /**
             * Account ID of the user who created this task.
             */
            createdBy?: string;
            /**
             * Account ID of the user to whom this task is assigned.
             */
            assignedTo?: string;
            /**
             * Account ID of the user who completed this task.
             */
            completedBy?: string;
            /**
             * Date and time when the task was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            /**
             * Date and time when the task was updated. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            updatedAt?: string; // date-time
            /**
             * Date and time when the task is due. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            dueAt?: string; // date-time
            /**
             * Date and time when the task was completed. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            completedAt?: string; // date-time
        }
        /**
         * Contains fields for each representation type requested.
         */
        export interface TaskBodySingle {
            storage?: BodyType;
            atlas_doc_format?: BodyType;
        }
        export interface UpdateFooterCommentModel {
            version?: {
                /**
                 * Number of new version. Should be 1 higher than current version of the comment.
                 */
                number?: number;
                /**
                 * Optional message store for the new version.
                 */
                message?: string;
            };
            body?: CommentBodyWrite | /**
             * Body of the comment. Only one body format should be specified as the property
             * for this object, e.g. `storage`.
             */
            CommentNestedBodyWrite;
        }
        export interface UpdateInlineCommentModel {
            version?: {
                /**
                 * Number of new version. Should be 1 higher than current version of the comment.
                 */
                number?: number;
                /**
                 * Optional message store for the new version.
                 */
                message?: string;
            };
            body?: CommentBodyWrite | /**
             * Body of the comment. Only one body format should be specified as the property
             * for this object, e.g. `storage`.
             */
            CommentNestedBodyWrite;
            /**
             * Resolved state of the comment. Set to true to resolve the comment, set to false to reopen it. If
             * matching the existing state (i.e. true -> resolved or false -> open/reopened) , no change will occur. A dangling
             * comment cannot be updated.
             */
            resolved?: boolean;
        }
        export interface Version {
            /**
             * Date and time when the version was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            /**
             * Message associated with the current version.
             */
            message?: string;
            /**
             * The version number.
             */
            number?: number; // int32
            /**
             * Describes if this version is a minor version. Email notifications and activity stream updates are not created for minor versions.
             */
            minorEdit?: boolean;
            /**
             * The account ID of the user who created this version.
             */
            authorId?: string;
        }
        /**
         * The sort fields for versions. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`.
         */
        export type VersionSortOrder = "modified-date" | "-modified-date";
        export interface VersionedEntity {
            /**
             * Title of the entity.
             */
            title?: string;
            /**
             * ID of the entity.
             */
            id?: string;
            body?: /* Contains fields for each representation type requested. */ BodyBulk;
        }
        export interface WhiteboardSingle {
            /**
             * ID of the whiteboard.
             */
            id?: string;
            /**
             * The content type of the object
             */
            type?: string;
            status?: /* The status of the content. */ ContentStatus;
            /**
             * Title of the whiteboard.
             */
            title?: string;
            /**
             * ID of the parent content, or null if there is no parent content.
             */
            parentId?: string;
            parentType?: /* Content type of the parent, or null if there is no parent. */ ParentContentType;
            /**
             * Position of whiteboard within the given parent page tree.
             */
            position?: number | null; // int32
            /**
             * The account ID of the user who created this whiteboard originally.
             */
            authorId?: string;
            /**
             * The account ID of the user who owns this whiteboard.
             */
            ownerId?: string;
            /**
             * Date and time when the whiteboard was created. In format "YYYY-MM-DDTHH:mm:ss.sssZ".
             */
            createdAt?: string; // date-time
            version?: Version;
            _links?: AbstractPageLinks;
        }
    }
}
export declare namespace Paths {
    namespace CheckAccessByEmail {
        export type RequestBody = Components.RequestBodies.CheckAccessOrInviteByEmailRequest;
        namespace Responses {
            export interface $200 {
                /**
                 * List of emails that do not have access to site.
                 */
                emailsWithoutAccess?: string[];
                /**
                 * List of invalid emails provided in the request.
                 */
                invalidEmails?: string[];
            }
            export interface $404 {
            }
            export interface $503 {
            }
        }
    }
    namespace ConvertContentIdsToContentTypes {
        export type RequestBody = Components.RequestBodies.ContentIdToContentTypeRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentIdToContentTypeResponse;
        }
    }
    namespace CreateAttachmentProperty {
        namespace Parameters {
            export type AttachmentId = string; // (att)?[0-9]+
        }
        export interface PathParameters {
            "attachment-id": Parameters.AttachmentId /* (att)?[0-9]+ */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyCreateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace CreateBlogPost {
        namespace Parameters {
            export type Private = boolean;
        }
        export interface QueryParameters {
            private?: Parameters.Private;
        }
        export type RequestBody = Components.RequestBodies.BlogPostCreateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.BlogPostSingle;
        }
    }
    namespace CreateBlogpostProperty {
        namespace Parameters {
            export type BlogpostId = number; // int64
        }
        export interface PathParameters {
            "blogpost-id": Parameters.BlogpostId /* int64 */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyCreateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace CreateCommentProperty {
        namespace Parameters {
            export type CommentId = number; // int64
        }
        export interface PathParameters {
            "comment-id": Parameters.CommentId /* int64 */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyCreateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace CreateCustomContent {
        export type RequestBody = Components.RequestBodies.CustomContentCreateRequest;
        namespace Responses {
            export type $201 = Components.Schemas.CustomContentSingle;
            export interface $404 {
            }
        }
    }
    namespace CreateCustomContentProperty {
        namespace Parameters {
            export type CustomContentId = number; // int64
        }
        export interface PathParameters {
            "custom-content-id": Parameters.CustomContentId /* int64 */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyCreateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace CreateFooterComment {
        export type RequestBody = Components.Schemas.CreateFooterCommentModel;
        namespace Responses {
            export type $201 = Components.Schemas.FooterCommentModel;
        }
    }
    namespace CreateInlineComment {
        export type RequestBody = Components.Schemas.CreateInlineCommentModel;
        namespace Responses {
            export type $201 = Components.Schemas.InlineCommentModel;
        }
    }
    namespace CreatePage {
        namespace Parameters {
            export type Embedded = boolean;
            export type Private = boolean;
            export type RootLevel = boolean;
        }
        export interface QueryParameters {
            embedded?: Parameters.Embedded;
            private?: Parameters.Private;
            "root-level"?: Parameters.RootLevel;
        }
        export type RequestBody = Components.RequestBodies.PageCreateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.PageSingle;
            export interface $404 {
            }
        }
    }
    namespace CreatePageProperty {
        namespace Parameters {
            export type PageId = number; // int64
        }
        export interface PathParameters {
            "page-id": Parameters.PageId /* int64 */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyCreateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace CreateSpaceProperty {
        namespace Parameters {
            export type SpaceId = number; // int64
        }
        export interface PathParameters {
            "space-id": Parameters.SpaceId /* int64 */;
        }
        export type RequestBody = Components.Schemas.SpacePropertyCreateRequest;
        namespace Responses {
            export type $201 = Components.Schemas.SpaceProperty;
        }
    }
    namespace CreateWhiteboard {
        namespace Parameters {
            export type Private = boolean;
        }
        export interface QueryParameters {
            private?: Parameters.Private;
        }
        export type RequestBody = Components.RequestBodies.WhiteboardCreateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.WhiteboardSingle;
            export interface $404 {
            }
        }
    }
    namespace CreateWhiteboardProperty {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyCreateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace DeleteAttachment {
        namespace Parameters {
            export type Id = number; // int64
            export type Purge = boolean;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            purge?: Parameters.Purge;
        }
        namespace Responses {
            export interface $404 {
            }
        }
    }
    namespace DeleteAttachmentPropertyById {
        namespace Parameters {
            export type AttachmentId = string; // (att)?[0-9]+
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "attachment-id": Parameters.AttachmentId /* (att)?[0-9]+ */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace DeleteBlogPost {
        namespace Parameters {
            export type Draft = boolean;
            export type Id = number; // int64
            export type Purge = boolean;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            purge?: Parameters.Purge;
            draft?: Parameters.Draft;
        }
        namespace Responses {
            export interface $404 {
            }
        }
    }
    namespace DeleteBlogpostPropertyById {
        namespace Parameters {
            export type BlogpostId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "blogpost-id": Parameters.BlogpostId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace DeleteCommentPropertyById {
        namespace Parameters {
            export type CommentId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "comment-id": Parameters.CommentId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace DeleteCustomContent {
        namespace Parameters {
            export type Id = number; // int64
            export type Purge = boolean;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            purge?: Parameters.Purge;
        }
        namespace Responses {
            export interface $404 {
            }
        }
    }
    namespace DeleteCustomContentPropertyById {
        namespace Parameters {
            export type CustomContentId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "custom-content-id": Parameters.CustomContentId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace DeleteFooterComment {
        namespace Parameters {
            export type CommentId = number; // int64
        }
        export interface PathParameters {
            "comment-id": Parameters.CommentId /* int64 */;
        }
        namespace Responses {
            export interface $204 {
            }
            export interface $404 {
            }
        }
    }
    namespace DeleteInlineComment {
        namespace Parameters {
            export type CommentId = number; // int64
        }
        export interface PathParameters {
            "comment-id": Parameters.CommentId /* int64 */;
        }
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace DeletePage {
        namespace Parameters {
            export type Draft = boolean;
            export type Id = number; // int64
            export type Purge = boolean;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            purge?: Parameters.Purge;
            draft?: Parameters.Draft;
        }
        namespace Responses {
            export interface $404 {
            }
        }
    }
    namespace DeletePagePropertyById {
        namespace Parameters {
            export type PageId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "page-id": Parameters.PageId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace DeleteSpacePropertyById {
        namespace Parameters {
            export type PropertyId = number; // int64
            export type SpaceId = number; // int64
        }
        export interface PathParameters {
            "space-id": Parameters.SpaceId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace DeleteWhiteboard {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            export interface $404 {
            }
        }
    }
    namespace DeleteWhiteboardPropertyById {
        namespace Parameters {
            export type PropertyId = number; // int64
            export type WhiteboardId = number; // int64
        }
        export interface PathParameters {
            "whiteboard-id": Parameters.WhiteboardId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace GetAttachmentById {
        namespace Parameters {
            export type Id = string; // (att)?[0-9]+
            export type Version = number;
        }
        export interface PathParameters {
            id: Parameters.Id /* (att)?[0-9]+ */;
        }
        export interface QueryParameters {
            version?: Parameters.Version;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Attachment;
        }
    }
    namespace GetAttachmentComments {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = string; // (att)?[0-9]+
            export type Limit = number; // int32
            export type Sort = /* The sort fields for comments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CommentSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* (att)?[0-9]+ */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            sort?: Parameters.Sort;
        }
        namespace Responses {
            /**
             * MultiEntityResult<AttachmentCommentModel>
             */
            export interface $200 {
                results?: Components.Schemas.AttachmentCommentModel[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetAttachmentContentProperties {
        namespace Parameters {
            export type AttachmentId = string; // (att)?[0-9]+
            export type Cursor = string;
            export type Key = string;
            export type Limit = number; // int32
            export type Sort = /* The sort fields for content properties. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.ContentPropertySortOrder;
        }
        export interface PathParameters {
            "attachment-id": Parameters.AttachmentId /* (att)?[0-9]+ */;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<ContentProperty>
             */
            export interface $200 {
                results?: Components.Schemas.ContentProperty[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetAttachmentContentPropertiesById {
        namespace Parameters {
            export type AttachmentId = string; // (att)?0-9+
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "attachment-id": Parameters.AttachmentId /* (att)?0-9+ */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace GetAttachmentLabels {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Prefix = "my" | "team" | "global" | "system";
            export type Sort = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            prefix?: Parameters.Prefix;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Label>
             */
            export interface $200 {
                results?: Components.Schemas.Label[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetAttachmentOperations {
        namespace Parameters {
            export type Id = string; // (att)?[0-9]+
        }
        export interface PathParameters {
            id: Parameters.Id /* (att)?[0-9]+ */;
        }
        namespace Responses {
            export type $200 = /* The list of operations permitted on entity. */ Components.Schemas.PermittedOperationsResponse;
        }
    }
    namespace GetAttachmentVersionDetails {
        namespace Parameters {
            export type AttachmentId = string; // (att)?0-9+
            export type VersionNumber = number; // int64
        }
        export interface PathParameters {
            "attachment-id": Parameters.AttachmentId /* (att)?0-9+ */;
            "version-number": Parameters.VersionNumber /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.DetailedVersion;
        }
    }
    namespace GetAttachmentVersions {
        namespace Parameters {
            export type Cursor = string;
            export type Id = string; // (att)?[0-9]+
            export type Limit = number; // int32
            export type Sort = /* The sort fields for versions. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.VersionSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* (att)?[0-9]+ */;
        }
        export interface QueryParameters {
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            sort?: Parameters.Sort;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Version>
             */
            export interface $200 {
                results?: Components.Schemas.AttachmentVersion[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetAttachments {
        namespace Parameters {
            export type Cursor = string;
            export type Filename = string;
            export type Limit = number; // int32
            export type MediaType = string;
            export type Sort = /* The sort fields for attachments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.AttachmentSortOrder;
            export type Status = ("current" | "archived" | "trashed")[];
        }
        export interface QueryParameters {
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            status?: Parameters.Status;
            mediaType?: Parameters.MediaType;
            filename?: Parameters.Filename;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Attachment>
             */
            export interface $200 {
                results?: Components.Schemas.Attachment[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetBlogPostById {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentationSingle;
            export type GetDraft = boolean;
            export type Id = number; // int64
            export type Version = number;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            "get-draft"?: Parameters.GetDraft;
            version?: Parameters.Version;
        }
        namespace Responses {
            export type $200 = Components.Schemas.BlogPostSingle;
        }
    }
    namespace GetBlogPostFooterComments {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for comments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CommentSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<BlogPostCommentModel>
             */
            export interface $200 {
                results?: Components.Schemas.BlogPostCommentModel[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetBlogPostInlineComments {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for comments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CommentSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<BlogPostInlineCommentModel>
             */
            export interface $200 {
                results?: Components.Schemas.BlogPostInlineCommentModel[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetBlogPostLabels {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Prefix = "my" | "team" | "global" | "system";
            export type Sort = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            prefix?: Parameters.Prefix;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Label>
             */
            export interface $200 {
                results?: Components.Schemas.Label[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetBlogPostLikeCount {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            /**
             * Integer
             */
            export interface $200 {
                /**
                 * The count number
                 */
                count?: number; // int64
            }
        }
    }
    namespace GetBlogPostLikeUsers {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<String>
             */
            export interface $200 {
                results?: Components.Schemas.Like[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetBlogPostOperations {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            export type $200 = /* The list of operations permitted on entity. */ Components.Schemas.PermittedOperationsResponse;
        }
    }
    namespace GetBlogPostVersionDetails {
        namespace Parameters {
            export type BlogpostId = number; // int64
            export type VersionNumber = number; // int64
        }
        export interface PathParameters {
            "blogpost-id": Parameters.BlogpostId /* int64 */;
            "version-number": Parameters.VersionNumber /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.DetailedVersion;
        }
    }
    namespace GetBlogPostVersions {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for versions. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.VersionSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            sort?: Parameters.Sort;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Version>
             */
            export interface $200 {
                results?: Components.Schemas.BlogPostVersion[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetBlogPosts {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
            export type Limit = number; // int32
            export type Sort = /* The sort fields for blog posts. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.BlogPostSortOrder;
            export type SpaceId = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
            export type Status = ("current" | "deleted" | "trashed")[];
            export type Title = string;
        }
        export interface QueryParameters {
            id?: Parameters.Id;
            "space-id"?: Parameters.SpaceId;
            sort?: Parameters.Sort;
            status?: Parameters.Status;
            title?: Parameters.Title;
            "body-format"?: Parameters.BodyFormat;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<BlogPost>
             */
            export interface $200 {
                results?: Components.Schemas.BlogPostBulk[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetBlogPostsInSpace {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for blog posts. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.BlogPostSortOrder;
            export type Status = ("current" | "deleted" | "trashed")[];
            export type Title = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            sort?: Parameters.Sort;
            status?: Parameters.Status;
            title?: Parameters.Title;
            "body-format"?: Parameters.BodyFormat;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<BlogPost>
             */
            export interface $200 {
                results?: Components.Schemas.BlogPostBulk[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
            export interface $404 {
            }
        }
    }
    namespace GetBlogpostAttachments {
        namespace Parameters {
            export type Cursor = string;
            export type Filename = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type MediaType = string;
            export type Sort = /* The sort fields for attachments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.AttachmentSortOrder;
            export type Status = ("current" | "archived" | "trashed")[];
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            status?: Parameters.Status;
            mediaType?: Parameters.MediaType;
            filename?: Parameters.Filename;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Attachment>
             */
            export interface $200 {
                results?: Components.Schemas.Attachment[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetBlogpostContentProperties {
        namespace Parameters {
            export type BlogpostId = number; // int64
            export type Cursor = string;
            export type Key = string;
            export type Limit = number; // int32
            export type Sort = /* The sort fields for content properties. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.ContentPropertySortOrder;
        }
        export interface PathParameters {
            "blogpost-id": Parameters.BlogpostId /* int64 */;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<ContentProperty>
             */
            export interface $200 {
                results?: Components.Schemas.ContentProperty[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetBlogpostContentPropertiesById {
        namespace Parameters {
            export type BlogpostId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "blogpost-id": Parameters.BlogpostId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace GetChildCustomContent {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            sort?: Parameters.Sort;
        }
        namespace Responses {
            /**
             * MultiEntityResult<ChildCustomContent>
             */
            export interface $200 {
                results?: Components.Schemas.ChildCustomContent[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetChildPages {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            sort?: Parameters.Sort;
        }
        namespace Responses {
            /**
             * MultiEntityResult<ChildPage>
             */
            export interface $200 {
                results?: Components.Schemas.ChildPage[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetCommentContentProperties {
        namespace Parameters {
            export type CommentId = number; // int64
            export type Cursor = string;
            export type Key = string;
            export type Limit = number; // int32
            export type Sort = /* The sort fields for content properties. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.ContentPropertySortOrder;
        }
        export interface PathParameters {
            "comment-id": Parameters.CommentId /* int64 */;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<ContentProperty>
             */
            export interface $200 {
                results?: Components.Schemas.ContentProperty[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetCommentContentPropertiesById {
        namespace Parameters {
            export type CommentId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "comment-id": Parameters.CommentId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace GetCustomContentAttachments {
        namespace Parameters {
            export type Cursor = string;
            export type Filename = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type MediaType = string;
            export type Sort = /* The sort fields for attachments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.AttachmentSortOrder;
            export type Status = ("current" | "archived" | "trashed")[];
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            status?: Parameters.Status;
            mediaType?: Parameters.MediaType;
            filename?: Parameters.Filename;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Attachment>
             */
            export interface $200 {
                results?: Components.Schemas.Attachment[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetCustomContentById {
        namespace Parameters {
            export type BodyFormat = /* The formats a custom content body can be represented as. A subset of BodyRepresentation. */ Components.Schemas.CustomContentBodyRepresentationSingle;
            export type Id = number; // int64
            export type Version = number;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            version?: Parameters.Version;
        }
        namespace Responses {
            export type $200 = Components.Schemas.CustomContentSingle;
        }
    }
    namespace GetCustomContentByType {
        namespace Parameters {
            export type BodyFormat = /* The formats a custom content body can be represented as. A subset of BodyRepresentation. */ Components.Schemas.CustomContentBodyRepresentation;
            export type Cursor = string;
            export type Id = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
            export type Limit = number; // int32
            export type Sort = /* The sort fields for custom content. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CustomContentSortOrder;
            export type SpaceId = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
            export type Type = string;
        }
        export interface QueryParameters {
            type: Parameters.Type;
            id?: Parameters.Id;
            "space-id"?: Parameters.SpaceId;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            "body-format"?: Parameters.BodyFormat;
        }
        namespace Responses {
            /**
             * MultiEntityResult<CustomContent>
             */
            export interface $200 {
                results?: Components.Schemas.CustomContentBulk[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
            export interface $404 {
            }
        }
    }
    namespace GetCustomContentByTypeInBlogPost {
        namespace Parameters {
            export type BodyFormat = /* The formats a custom content body can be represented as. A subset of BodyRepresentation. */ Components.Schemas.CustomContentBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for custom content. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CustomContentSortOrder;
            export type Type = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            type: Parameters.Type;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            "body-format"?: Parameters.BodyFormat;
        }
        namespace Responses {
            /**
             * MultiEntityResult<CustomContent>
             */
            export interface $200 {
                results?: Components.Schemas.CustomContentBulk[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
            export interface $404 {
            }
        }
    }
    namespace GetCustomContentByTypeInPage {
        namespace Parameters {
            export type BodyFormat = /* The formats a custom content body can be represented as. A subset of BodyRepresentation. */ Components.Schemas.CustomContentBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for custom content. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CustomContentSortOrder;
            export type Type = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            type: Parameters.Type;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            "body-format"?: Parameters.BodyFormat;
        }
        namespace Responses {
            /**
             * MultiEntityResult<CustomContent>
             */
            export interface $200 {
                results?: Components.Schemas.CustomContentBulk[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
            export interface $404 {
            }
        }
    }
    namespace GetCustomContentByTypeInSpace {
        namespace Parameters {
            export type BodyFormat = /* The formats a custom content body can be represented as. A subset of BodyRepresentation. */ Components.Schemas.CustomContentBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Type = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            type: Parameters.Type;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            "body-format"?: Parameters.BodyFormat;
        }
        namespace Responses {
            /**
             * MultiEntityResult<CustomContent>
             */
            export interface $200 {
                results?: Components.Schemas.CustomContentBulk[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
            export interface $404 {
            }
        }
    }
    namespace GetCustomContentComments {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for comments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CommentSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            sort?: Parameters.Sort;
        }
        namespace Responses {
            /**
             * MultiEntityResult<CustomContentCommentModel>
             */
            export interface $200 {
                results?: Components.Schemas.CustomContentCommentModel[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetCustomContentContentProperties {
        namespace Parameters {
            export type Cursor = string;
            export type CustomContentId = number; // int64
            export type Key = string;
            export type Limit = number; // int32
            export type Sort = /* The sort fields for content properties. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.ContentPropertySortOrder;
        }
        export interface PathParameters {
            "custom-content-id": Parameters.CustomContentId /* int64 */;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<ContentProperty>
             */
            export interface $200 {
                results?: Components.Schemas.ContentProperty[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetCustomContentContentPropertiesById {
        namespace Parameters {
            export type CustomContentId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "custom-content-id": Parameters.CustomContentId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace GetCustomContentLabels {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Prefix = "my" | "team" | "global" | "system";
            export type Sort = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            prefix?: Parameters.Prefix;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Label>
             */
            export interface $200 {
                results?: Components.Schemas.Label[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetCustomContentOperations {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            export type $200 = /* The list of operations permitted on entity. */ Components.Schemas.PermittedOperationsResponse;
        }
    }
    namespace GetCustomContentVersionDetails {
        namespace Parameters {
            export type CustomContentId = number; // int64
            export type VersionNumber = number; // int64
        }
        export interface PathParameters {
            "custom-content-id": Parameters.CustomContentId /* int64 */;
            "version-number": Parameters.VersionNumber /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.DetailedVersion;
        }
    }
    namespace GetCustomContentVersions {
        namespace Parameters {
            export type BodyFormat = /* The formats a custom content body can be represented as. A subset of BodyRepresentation. */ Components.Schemas.CustomContentBodyRepresentation;
            export type Cursor = string;
            export type CustomContentId = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for versions. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.VersionSortOrder;
        }
        export interface PathParameters {
            "custom-content-id": Parameters.CustomContentId /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            sort?: Parameters.Sort;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Version>
             */
            export interface $200 {
                results?: Components.Schemas.CustomContentVersion[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetDataPolicyMetadata {
        namespace Responses {
            export type $200 = /* Details about data policies. */ Components.Schemas.DataPolicyMetadata;
            export interface $400 {
            }
            export interface $401 {
            }
        }
    }
    namespace GetDataPolicySpaces {
        namespace Parameters {
            export type Cursor = string;
            export type Ids = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
            export type Keys = [
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?
            ];
            export type Limit = number; // int32
            export type Sort = /* The sort fields for spaces. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.SpaceSortOrder;
        }
        export interface QueryParameters {
            ids?: Parameters.Ids;
            keys?: Parameters.Keys;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<DataPolicySpace>
             */
            export interface $200 {
                results?: Components.Schemas.DataPolicySpace[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetFooterCommentById {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentationSingle;
            export type CommentId = number; // int64
            export type Version = number;
        }
        export interface PathParameters {
            "comment-id": Parameters.CommentId /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            version?: Parameters.Version;
        }
        namespace Responses {
            export type $200 = Components.Schemas.FooterCommentModel;
        }
    }
    namespace GetFooterCommentChildren {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for comments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CommentSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<ChildrenCommentModel>
             */
            export interface $200 {
                results?: Components.Schemas.ChildrenCommentModel[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetFooterCommentOperations {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            export type $200 = /* The list of operations permitted on entity. */ Components.Schemas.PermittedOperationsResponse;
        }
    }
    namespace GetFooterCommentVersionDetails {
        namespace Parameters {
            export type Id = number; // int64
            export type VersionNumber = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
            "version-number": Parameters.VersionNumber /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.DetailedVersion;
        }
    }
    namespace GetFooterCommentVersions {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for versions. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.VersionSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            sort?: Parameters.Sort;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Version>
             */
            export interface $200 {
                results?: Components.Schemas.CommentVersion[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetFooterComments {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Limit = number; // int32
            export type Sort = /* The sort fields for comments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CommentSortOrder;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<FooterCommentModel>
             */
            export interface $200 {
                results?: Components.Schemas.FooterCommentModel[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetFooterLikeCount {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            /**
             * Integer
             */
            export interface $200 {
                /**
                 * The count number
                 */
                count?: number; // int64
            }
        }
    }
    namespace GetFooterLikeUsers {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<String>
             */
            export interface $200 {
                results?: Components.Schemas.Like[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetInlineCommentById {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentationSingle;
            export type CommentId = number; // int64
            export type Version = number;
        }
        export interface PathParameters {
            "comment-id": Parameters.CommentId /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            version?: Parameters.Version;
        }
        namespace Responses {
            export type $200 = Components.Schemas.InlineCommentModel;
        }
    }
    namespace GetInlineCommentChildren {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for comments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CommentSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<InlineCommentChildrenModel>
             */
            export interface $200 {
                results?: Components.Schemas.InlineCommentChildrenModel[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetInlineCommentOperations {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            export type $200 = /* The list of operations permitted on entity. */ Components.Schemas.PermittedOperationsResponse;
        }
    }
    namespace GetInlineCommentVersionDetails {
        namespace Parameters {
            export type Id = number; // int64
            export type VersionNumber = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
            "version-number": Parameters.VersionNumber /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.DetailedVersion;
        }
    }
    namespace GetInlineCommentVersions {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for versions. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.VersionSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            sort?: Parameters.Sort;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Version>
             */
            export interface $200 {
                results?: Components.Schemas.CommentVersion[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetInlineComments {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Limit = number; // int32
            export type Sort = /* The sort fields for comments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CommentSortOrder;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<InlineCommentModel>
             */
            export interface $200 {
                results?: Components.Schemas.InlineCommentModel[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetInlineLikeCount {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            /**
             * Integer
             */
            export interface $200 {
                /**
                 * The count number
                 */
                count?: number; // int64
            }
        }
    }
    namespace GetInlineLikeUsers {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<String>
             */
            export interface $200 {
                results?: Components.Schemas.Like[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetLabelAttachments {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for attachments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.AttachmentSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Attachment>
             */
            export interface $200 {
                results?: Components.Schemas.Attachment[];
            }
        }
    }
    namespace GetLabelBlogPosts {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for blog posts. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.BlogPostSortOrder;
            export type SpaceId = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "space-id"?: Parameters.SpaceId;
            "body-format"?: Parameters.BodyFormat;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<BlogPost>
             */
            export interface $200 {
                results?: Components.Schemas.BlogPostBulk[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetLabelPages {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for pages. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.PageSortOrder;
            export type SpaceId = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "space-id"?: Parameters.SpaceId;
            "body-format"?: Parameters.BodyFormat;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Page>
             */
            export interface $200 {
                results?: Components.Schemas.PageBulk[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetLabels {
        namespace Parameters {
            export type Cursor = string;
            export type LabelId = number /* int64 */[];
            export type Limit = number; // int32
            export type Prefix = string[];
            export type Sort = string;
        }
        export interface QueryParameters {
            "label-id"?: Parameters.LabelId;
            prefix?: Parameters.Prefix;
            cursor?: Parameters.Cursor;
            sort?: Parameters.Sort;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Label>
             */
            export interface $200 {
                results?: Components.Schemas.Label[];
            }
        }
    }
    namespace GetPageAncestors {
        namespace Parameters {
            export type Id = number; // int64
            export type Limit = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Ancestor>
             */
            export interface $200 {
                results?: Components.Schemas.Ancestor[];
            }
            export interface $404 {
            }
        }
    }
    namespace GetPageAttachments {
        namespace Parameters {
            export type Cursor = string;
            export type Filename = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type MediaType = string;
            export type Sort = /* The sort fields for attachments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.AttachmentSortOrder;
            export type Status = ("current" | "archived" | "trashed")[];
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            status?: Parameters.Status;
            mediaType?: Parameters.MediaType;
            filename?: Parameters.Filename;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Attachment>
             */
            export interface $200 {
                results?: Components.Schemas.Attachment[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetPageById {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentationSingle;
            export type GetDraft = boolean;
            export type Id = number; // int64
            export type Version = number;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            "get-draft"?: Parameters.GetDraft;
            version?: Parameters.Version;
        }
        namespace Responses {
            export type $200 = Components.Schemas.PageSingle;
        }
    }
    namespace GetPageContentProperties {
        namespace Parameters {
            export type Cursor = string;
            export type Key = string;
            export type Limit = number; // int32
            export type PageId = number; // int64
            export type Sort = /* The sort fields for content properties. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.ContentPropertySortOrder;
        }
        export interface PathParameters {
            "page-id": Parameters.PageId /* int64 */;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<ContentProperty>
             */
            export interface $200 {
                results?: Components.Schemas.ContentProperty[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetPageContentPropertiesById {
        namespace Parameters {
            export type PageId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "page-id": Parameters.PageId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace GetPageFooterComments {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for comments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CommentSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<PageCommentModel>
             */
            export interface $200 {
                results?: Components.Schemas.PageCommentModel[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetPageInlineComments {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for comments. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.CommentSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<PageInlineCommentModel>
             */
            export interface $200 {
                results?: Components.Schemas.PageInlineCommentModel[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetPageLabels {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Prefix = "my" | "team" | "global" | "system";
            export type Sort = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            prefix?: Parameters.Prefix;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Label>
             */
            export interface $200 {
                results?: Components.Schemas.Label[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetPageLikeCount {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            /**
             * Integer
             */
            export interface $200 {
                /**
                 * The count number
                 */
                count?: number; // int64
            }
        }
    }
    namespace GetPageLikeUsers {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<String>
             */
            export interface $200 {
                results?: Components.Schemas.Like[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetPageOperations {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            export type $200 = /* The list of operations permitted on entity. */ Components.Schemas.PermittedOperationsResponse;
        }
    }
    namespace GetPageVersionDetails {
        namespace Parameters {
            export type PageId = number; // int64
            export type VersionNumber = number; // int64
        }
        export interface PathParameters {
            "page-id": Parameters.PageId /* int64 */;
            "version-number": Parameters.VersionNumber /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.DetailedVersion;
        }
    }
    namespace GetPageVersions {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for versions. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.VersionSortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
            sort?: Parameters.Sort;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Version>
             */
            export interface $200 {
                results?: Components.Schemas.PageVersion[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetPages {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Id = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
            export type Limit = number; // int32
            export type Sort = /* The sort fields for pages. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.PageSortOrder;
            export type SpaceId = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
            export type Status = ("current" | "archived" | "deleted" | "trashed")[];
            export type Title = string;
        }
        export interface QueryParameters {
            id?: Parameters.Id;
            "space-id"?: Parameters.SpaceId;
            sort?: Parameters.Sort;
            status?: Parameters.Status;
            title?: Parameters.Title;
            "body-format"?: Parameters.BodyFormat;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Page>
             */
            export interface $200 {
                results?: Components.Schemas.PageBulk[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetPagesInSpace {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Cursor = string;
            export type Depth = "all" | "root";
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Sort = /* The sort fields for pages. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.PageSortOrder;
            export type Status = ("current" | "archived" | "deleted" | "trashed")[];
            export type Title = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            depth?: Parameters.Depth;
            sort?: Parameters.Sort;
            status?: Parameters.Status;
            title?: Parameters.Title;
            "body-format"?: Parameters.BodyFormat;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Page>
             */
            export interface $200 {
                results?: Components.Schemas.PageBulk[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetSpaceById {
        namespace Parameters {
            export type DescriptionFormat = /* The formats a space description can be represented as. A subset of BodyRepresentation. */ Components.Schemas.SpaceDescriptionBodyRepresentation;
            export type Id = number; // int64
            export type IncludeIcon = boolean;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "description-format"?: Parameters.DescriptionFormat;
            "include-icon"?: Parameters.IncludeIcon;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Space;
        }
    }
    namespace GetSpaceContentLabels {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Prefix = "my" | "team";
            export type Sort = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            prefix?: Parameters.Prefix;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Label>
             */
            export interface $200 {
                results?: Components.Schemas.Label[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetSpaceLabels {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
            export type Prefix = "my" | "team";
            export type Sort = string;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            prefix?: Parameters.Prefix;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Label>
             */
            export interface $200 {
                results?: Components.Schemas.Label[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetSpaceOperations {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            export type $200 = /* The list of operations permitted on entity. */ Components.Schemas.PermittedOperationsResponse;
        }
    }
    namespace GetSpacePermissions {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Limit = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<SpacePermission>
             */
            export interface $200 {
                results?: Components.Schemas.SpacePermission[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetSpaceProperties {
        namespace Parameters {
            export type Cursor = string;
            export type Key = string;
            export type Limit = number; // int32
            export type SpaceId = number; // int64
        }
        export interface PathParameters {
            "space-id": Parameters.SpaceId /* int64 */;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<SpaceProperty>
             */
            export interface $200 {
                results?: Components.Schemas.SpaceProperty[];
            }
        }
    }
    namespace GetSpacePropertyById {
        namespace Parameters {
            export type PropertyId = number; // int64
            export type SpaceId = number; // int64
        }
        export interface PathParameters {
            "space-id": Parameters.SpaceId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.SpaceProperty;
        }
    }
    namespace GetSpaces {
        namespace Parameters {
            export type Cursor = string;
            export type DescriptionFormat = /* The formats a space description can be represented as. A subset of BodyRepresentation. */ Components.Schemas.SpaceDescriptionBodyRepresentation;
            export type FavoritedBy = string;
            export type Ids = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
            export type IncludeIcon = boolean;
            export type Keys = [
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?
            ];
            export type Labels = string[];
            export type Limit = number; // int32
            export type NotFavoritedBy = string;
            export type Sort = /* The sort fields for spaces. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.SpaceSortOrder;
            export type Status = "current" | "archived";
            export type Type = "global" | "personal";
        }
        export interface QueryParameters {
            ids?: Parameters.Ids;
            keys?: Parameters.Keys;
            type?: Parameters.Type;
            status?: Parameters.Status;
            labels?: Parameters.Labels;
            "favorited-by"?: Parameters.FavoritedBy;
            "not-favorited-by"?: Parameters.NotFavoritedBy;
            sort?: Parameters.Sort;
            "description-format"?: Parameters.DescriptionFormat;
            "include-icon"?: Parameters.IncludeIcon;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Space>
             */
            export interface $200 {
                results?: Components.Schemas.Space[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetTaskById {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Task;
        }
    }
    namespace GetTasks {
        namespace Parameters {
            export type AssignedTo = [
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?
            ];
            export type BlogpostId = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type CompletedAtFrom = number; // int64
            export type CompletedAtTo = number; // int64
            export type CompletedBy = [
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?
            ];
            export type CreatedAtFrom = number; // int64
            export type CreatedAtTo = number; // int64
            export type CreatedBy = [
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?,
                string?
            ];
            export type Cursor = string;
            export type DueAtFrom = number; // int64
            export type DueAtTo = number; // int64
            export type IncludeBlankTasks = boolean;
            export type Limit = number; // int32
            export type PageId = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
            export type SpaceId = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
            export type Status = "complete" | "incomplete";
            export type TaskId = [
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?,
                number?
            ];
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
            "include-blank-tasks"?: Parameters.IncludeBlankTasks;
            status?: Parameters.Status;
            "task-id"?: Parameters.TaskId;
            "space-id"?: Parameters.SpaceId;
            "page-id"?: Parameters.PageId;
            "blogpost-id"?: Parameters.BlogpostId;
            "created-by"?: Parameters.CreatedBy;
            "assigned-to"?: Parameters.AssignedTo;
            "completed-by"?: Parameters.CompletedBy;
            "created-at-from"?: Parameters.CreatedAtFrom /* int64 */;
            "created-at-to"?: Parameters.CreatedAtTo /* int64 */;
            "due-at-from"?: Parameters.DueAtFrom /* int64 */;
            "due-at-to"?: Parameters.DueAtTo /* int64 */;
            "completed-at-from"?: Parameters.CompletedAtFrom /* int64 */;
            "completed-at-to"?: Parameters.CompletedAtTo /* int64 */;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Task>
             */
            export interface $200 {
                results?: Components.Schemas.Task[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetWhiteboardAncestors {
        namespace Parameters {
            export type Id = number; // int64
            export type Limit = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<Ancestor>
             */
            export interface $200 {
                results?: Components.Schemas.Ancestor[];
            }
            export interface $404 {
            }
        }
    }
    namespace GetWhiteboardById {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.WhiteboardSingle;
        }
    }
    namespace GetWhiteboardContentProperties {
        namespace Parameters {
            export type Cursor = string;
            export type Id = number; // int64
            export type Key = string;
            export type Limit = number; // int32
            export type Sort = /* The sort fields for content properties. The default sort direction is ascending. To sort in descending order, append a `-` character before the sort field. For example, `fieldName` or `-fieldName`. */ Components.Schemas.ContentPropertySortOrder;
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            sort?: Parameters.Sort;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            /**
             * MultiEntityResult<ContentProperty>
             */
            export interface $200 {
                results?: Components.Schemas.ContentProperty[];
                _links?: {
                    /**
                     * Used for pagination. Contains the relative URL for the next set of results, using a cursor query parameter.
                     * This property will not be present if there is no additional data available.
                     */
                    next?: string;
                };
            }
        }
    }
    namespace GetWhiteboardContentPropertiesById {
        namespace Parameters {
            export type PropertyId = number; // int64
            export type WhiteboardId = number; // int64
        }
        export interface PathParameters {
            "whiteboard-id": Parameters.WhiteboardId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace GetWhiteboardOperations {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        namespace Responses {
            export type $200 = /* The list of operations permitted on entity. */ Components.Schemas.PermittedOperationsResponse;
        }
    }
    namespace InviteByEmail {
        export type RequestBody = Components.RequestBodies.CheckAccessOrInviteByEmailRequest;
        namespace Responses {
            export interface $404 {
            }
            export interface $503 {
            }
        }
    }
    namespace UpdateAttachmentPropertyById {
        namespace Parameters {
            export type AttachmentId = string; // (att)?[0-9]+
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "attachment-id": Parameters.AttachmentId /* (att)?[0-9]+ */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyUpdateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace UpdateBlogPost {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export type RequestBody = Components.RequestBodies.BlogPostUpdateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.BlogPostSingle;
            export interface $404 {
            }
        }
    }
    namespace UpdateBlogpostPropertyById {
        namespace Parameters {
            export type BlogpostId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "blogpost-id": Parameters.BlogpostId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyUpdateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace UpdateCommentPropertyById {
        namespace Parameters {
            export type CommentId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "comment-id": Parameters.CommentId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyUpdateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace UpdateCustomContent {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export type RequestBody = Components.RequestBodies.CustomContentUpdateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.CustomContentSingle;
            export interface $404 {
            }
        }
    }
    namespace UpdateCustomContentPropertyById {
        namespace Parameters {
            export type CustomContentId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "custom-content-id": Parameters.CustomContentId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyUpdateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace UpdateFooterComment {
        namespace Parameters {
            export type CommentId = number; // int64
        }
        export interface PathParameters {
            "comment-id": Parameters.CommentId /* int64 */;
        }
        export type RequestBody = Components.Schemas.UpdateFooterCommentModel;
        namespace Responses {
            export type $200 = Components.Schemas.FooterCommentModel;
            export interface $404 {
            }
        }
    }
    namespace UpdateInlineComment {
        namespace Parameters {
            export type CommentId = number; // int64
        }
        export interface PathParameters {
            "comment-id": Parameters.CommentId /* int64 */;
        }
        export type RequestBody = Components.Schemas.UpdateInlineCommentModel;
        namespace Responses {
            export type $200 = Components.Schemas.InlineCommentModel;
        }
    }
    namespace UpdatePage {
        namespace Parameters {
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export type RequestBody = Components.RequestBodies.PageUpdateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.PageSingle;
            export interface $404 {
            }
        }
    }
    namespace UpdatePagePropertyById {
        namespace Parameters {
            export type PageId = number; // int64
            export type PropertyId = number; // int64
        }
        export interface PathParameters {
            "page-id": Parameters.PageId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyUpdateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace UpdateSpacePropertyById {
        namespace Parameters {
            export type PropertyId = number; // int64
            export type SpaceId = number; // int64
        }
        export interface PathParameters {
            "space-id": Parameters.SpaceId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        export type RequestBody = Components.Schemas.SpacePropertyUpdateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.SpaceProperty;
        }
    }
    namespace UpdateTask {
        namespace Parameters {
            export type BodyFormat = /* The primary formats a body can be represented as. A subset of BodyRepresentation. These formats are the only allowed formats in certain use cases. */ Components.Schemas.PrimaryBodyRepresentation;
            export type Id = number; // int64
        }
        export interface PathParameters {
            id: Parameters.Id /* int64 */;
        }
        export interface QueryParameters {
            "body-format"?: Parameters.BodyFormat;
        }
        export type RequestBody = Components.RequestBodies.TaskUpdateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.Task;
        }
    }
    namespace UpdateWhiteboardPropertyById {
        namespace Parameters {
            export type PropertyId = number; // int64
            export type WhiteboardId = number; // int64
        }
        export interface PathParameters {
            "whiteboard-id": Parameters.WhiteboardId /* int64 */;
            "property-id": Parameters.PropertyId /* int64 */;
        }
        export type RequestBody = Components.Schemas.ContentPropertyUpdateRequest;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
}

export interface OperationMethods {
  /**
   * getAttachments - Get attachments
   * 
   * Returns all attachments. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the container of the attachment.
   */
  'getAttachments'(
    parameters?: Parameters<Paths.GetAttachments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAttachments.Responses.$200>
  /**
   * getAttachmentById - Get attachment by id
   * 
   * Returns a specific attachment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the attachment's container.
   */
  'getAttachmentById'(
    parameters?: Parameters<Paths.GetAttachmentById.PathParameters & Paths.GetAttachmentById.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAttachmentById.Responses.$200>
  /**
   * deleteAttachment - Delete attachment
   * 
   * Delete an attachment by id.
   * 
   * Deleting an attachment moves the attachment to the trash, where it can be restored later. To permanently delete an attachment (or "purge" it),
   * the endpoint must be called on a **trashed** attachment with the following param `purge=true`.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the container of the attachment.
   * Permission to delete attachments in the space.
   * Permission to administer the space (if attempting to purge).
   */
  'deleteAttachment'(
    parameters?: Parameters<Paths.DeleteAttachment.PathParameters & Paths.DeleteAttachment.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getAttachmentLabels - Get labels for attachment
   * 
   * Returns the labels of specific attachment. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the parent content of the attachment and its corresponding space.
   */
  'getAttachmentLabels'(
    parameters?: Parameters<Paths.GetAttachmentLabels.PathParameters & Paths.GetAttachmentLabels.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAttachmentLabels.Responses.$200>
  /**
   * getAttachmentOperations - Get permitted operations for attachment
   * 
   * Returns the permitted operations on specific attachment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the parent content of the attachment and its corresponding space.
   */
  'getAttachmentOperations'(
    parameters?: Parameters<Paths.GetAttachmentOperations.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAttachmentOperations.Responses.$200>
  /**
   * getAttachmentContentProperties - Get content properties for attachment
   * 
   * Retrieves all Content Properties tied to a specified attachment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the attachment.
   */
  'getAttachmentContentProperties'(
    parameters?: Parameters<Paths.GetAttachmentContentProperties.PathParameters & Paths.GetAttachmentContentProperties.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAttachmentContentProperties.Responses.$200>
  /**
   * createAttachmentProperty - Create content property for attachment
   * 
   * Creates a new content property for an attachment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the attachment.
   */
  'createAttachmentProperty'(
    parameters?: Parameters<Paths.CreateAttachmentProperty.PathParameters> | null,
    data?: Paths.CreateAttachmentProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateAttachmentProperty.Responses.$200>
  /**
   * getAttachmentContentPropertiesById - Get content property for attachment by id
   * 
   * Retrieves a specific Content Property by ID that is attached to a specified attachment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the attachment.
   */
  'getAttachmentContentPropertiesById'(
    parameters?: Parameters<Paths.GetAttachmentContentPropertiesById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAttachmentContentPropertiesById.Responses.$200>
  /**
   * updateAttachmentPropertyById - Update content property for attachment by id
   * 
   * Update a content property for attachment by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the attachment.
   */
  'updateAttachmentPropertyById'(
    parameters?: Parameters<Paths.UpdateAttachmentPropertyById.PathParameters> | null,
    data?: Paths.UpdateAttachmentPropertyById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateAttachmentPropertyById.Responses.$200>
  /**
   * deleteAttachmentPropertyById - Delete content property for attachment by id
   * 
   * Deletes a content property for an attachment by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to attachment the page.
   */
  'deleteAttachmentPropertyById'(
    parameters?: Parameters<Paths.DeleteAttachmentPropertyById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeleteAttachmentPropertyById.Responses.$204>
  /**
   * getAttachmentVersions - Get attachment versions
   * 
   * Returns the versions of specific attachment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the attachment and its corresponding space.
   */
  'getAttachmentVersions'(
    parameters?: Parameters<Paths.GetAttachmentVersions.PathParameters & Paths.GetAttachmentVersions.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAttachmentVersions.Responses.$200>
  /**
   * getAttachmentVersionDetails - Get version details for attachment version
   * 
   * Retrieves version details for the specified attachment and version number.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the attachment.
   */
  'getAttachmentVersionDetails'(
    parameters?: Parameters<Paths.GetAttachmentVersionDetails.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAttachmentVersionDetails.Responses.$200>
  /**
   * getAttachmentComments - Get attachment comments
   * 
   * Returns the comments of the specific attachment.
   * The number of results is limited by the `limit` parameter and additional results (if available) will be available through
   * the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the attachment and its corresponding containers.
   */
  'getAttachmentComments'(
    parameters?: Parameters<Paths.GetAttachmentComments.PathParameters & Paths.GetAttachmentComments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAttachmentComments.Responses.$200>
  /**
   * getBlogPosts - Get blog posts
   * 
   * Returns all blog posts. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * Only blog posts that the user has permission to view will be returned.
   */
  'getBlogPosts'(
    parameters?: Parameters<Paths.GetBlogPosts.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogPosts.Responses.$200>
  /**
   * createBlogPost - Create blog post
   * 
   * Creates a new blog post in the space specified by the spaceId.
   * 
   * By default this will create the blog post as a non-draft, unless the status is specified as draft.
   * If creating a non-draft, the title must not be empty.
   * 
   * Currently only supports the storage representation specified in the body.representation enums below
   */
  'createBlogPost'(
    parameters?: Parameters<Paths.CreateBlogPost.QueryParameters> | null,
    data?: Paths.CreateBlogPost.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateBlogPost.Responses.$200>
  /**
   * getBlogPostById - Get blog post by id
   * 
   * Returns a specific blog post.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the blog post and its corresponding space.
   */
  'getBlogPostById'(
    parameters?: Parameters<Paths.GetBlogPostById.PathParameters & Paths.GetBlogPostById.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogPostById.Responses.$200>
  /**
   * updateBlogPost - Update blog post
   * 
   * Update a blog post by id.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the blog post and its corresponding space. Permission to update blog posts in the space.
   */
  'updateBlogPost'(
    parameters?: Parameters<Paths.UpdateBlogPost.PathParameters> | null,
    data?: Paths.UpdateBlogPost.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateBlogPost.Responses.$200>
  /**
   * deleteBlogPost - Delete blog post
   * 
   * Delete a blog post by id.
   * 
   * By default this will delete blog posts that are non-drafts. To delete a blog post that is a draft, the endpoint must be called on a 
   * draft with the following param `draft=true`. Discarded drafts are not sent to the trash and are permanently deleted.
   * 
   * Deleting a blog post that is not a draft moves the blog post to the trash, where it can be restored later. 
   * To permanently delete a blog post (or "purge" it), the endpoint must be called on a **trashed** blog post with the following param `purge=true`.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the blog post and its corresponding space.
   * Permission to delete blog posts in the space.
   * Permission to administer the space (if attempting to purge).
   */
  'deleteBlogPost'(
    parameters?: Parameters<Paths.DeleteBlogPost.PathParameters & Paths.DeleteBlogPost.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getBlogpostAttachments - Get attachments for blog post
   * 
   * Returns the attachments of specific blog post. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the blog post and its corresponding space.
   */
  'getBlogpostAttachments'(
    parameters?: Parameters<Paths.GetBlogpostAttachments.PathParameters & Paths.GetBlogpostAttachments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogpostAttachments.Responses.$200>
  /**
   * getCustomContentByTypeInBlogPost - Get custom content by type in blog post
   * 
   * Returns all custom content for a given type within a given blogpost. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the custom content, the container of the custom content (blog post), and the corresponding space.
   */
  'getCustomContentByTypeInBlogPost'(
    parameters?: Parameters<Paths.GetCustomContentByTypeInBlogPost.PathParameters & Paths.GetCustomContentByTypeInBlogPost.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentByTypeInBlogPost.Responses.$200>
  /**
   * getBlogPostLabels - Get labels for blog post
   * 
   * Returns the labels of specific blog post. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the blog post and its corresponding space.
   */
  'getBlogPostLabels'(
    parameters?: Parameters<Paths.GetBlogPostLabels.PathParameters & Paths.GetBlogPostLabels.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogPostLabels.Responses.$200>
  /**
   * getBlogPostLikeCount - Get like count for blog post
   * 
   * Returns the count of likes of specific blog post.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the blog post and its corresponding space.
   */
  'getBlogPostLikeCount'(
    parameters?: Parameters<Paths.GetBlogPostLikeCount.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogPostLikeCount.Responses.$200>
  /**
   * getBlogPostLikeUsers - Get account IDs of likes for blog post
   * 
   * Returns the account IDs of likes of specific blog post.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the blog post and its corresponding space.
   */
  'getBlogPostLikeUsers'(
    parameters?: Parameters<Paths.GetBlogPostLikeUsers.PathParameters & Paths.GetBlogPostLikeUsers.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogPostLikeUsers.Responses.$200>
  /**
   * getBlogpostContentProperties - Get content properties for blog post
   * 
   * Retrieves all Content Properties tied to a specified blog post.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the blog post.
   */
  'getBlogpostContentProperties'(
    parameters?: Parameters<Paths.GetBlogpostContentProperties.PathParameters & Paths.GetBlogpostContentProperties.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogpostContentProperties.Responses.$200>
  /**
   * createBlogpostProperty - Create content property for blog post
   * 
   * Creates a new property for a blogpost.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the blog post.
   */
  'createBlogpostProperty'(
    parameters?: Parameters<Paths.CreateBlogpostProperty.PathParameters> | null,
    data?: Paths.CreateBlogpostProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateBlogpostProperty.Responses.$200>
  /**
   * getBlogpostContentPropertiesById - Get content property for blog post by id
   * 
   * Retrieves a specific Content Property by ID that is attached to a specified blog post.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the blog post.
   */
  'getBlogpostContentPropertiesById'(
    parameters?: Parameters<Paths.GetBlogpostContentPropertiesById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogpostContentPropertiesById.Responses.$200>
  /**
   * updateBlogpostPropertyById - Update content property for blog post by id
   * 
   * Update a content property for blog post by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the blog post.
   */
  'updateBlogpostPropertyById'(
    parameters?: Parameters<Paths.UpdateBlogpostPropertyById.PathParameters> | null,
    data?: Paths.UpdateBlogpostPropertyById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateBlogpostPropertyById.Responses.$200>
  /**
   * deleteBlogpostPropertyById - Delete content property for blogpost by id
   * 
   * Deletes a content property for a blogpost by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the blog post.
   */
  'deleteBlogpostPropertyById'(
    parameters?: Parameters<Paths.DeleteBlogpostPropertyById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeleteBlogpostPropertyById.Responses.$204>
  /**
   * getBlogPostOperations - Get permitted operations for blog post
   * 
   * Returns the permitted operations on specific blog post.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the parent content of the blog post and its corresponding space.
   */
  'getBlogPostOperations'(
    parameters?: Parameters<Paths.GetBlogPostOperations.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogPostOperations.Responses.$200>
  /**
   * getBlogPostVersions - Get blog post versions
   * 
   * Returns the versions of specific blog post. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the blog post and its corresponding space.
   */
  'getBlogPostVersions'(
    parameters?: Parameters<Paths.GetBlogPostVersions.PathParameters & Paths.GetBlogPostVersions.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogPostVersions.Responses.$200>
  /**
   * getBlogPostVersionDetails - Get version details for blog post version
   * 
   * Retrieves version details for the specified blog post and version number.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the blog post.
   */
  'getBlogPostVersionDetails'(
    parameters?: Parameters<Paths.GetBlogPostVersionDetails.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogPostVersionDetails.Responses.$200>
  /**
   * convertContentIdsToContentTypes - Convert content ids to content types
   * 
   * Converts a list of content ids into their associated content types. This is useful for users migrating from v1 to v2
   * who may have stored just content ids without their associated type. This will return types as they should be used in v2.
   * Notably, this will return `inline-comment` for inline comments and `footer-comment` for footer comments, which is distinct from them
   * both being represented by `comment` in v1.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the requested content. Any content that the user does not have permission to view or does not exist will map to `null` in the response.
   */
  'convertContentIdsToContentTypes'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.ConvertContentIdsToContentTypes.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.ConvertContentIdsToContentTypes.Responses.$200>
  /**
   * getCustomContentByType - Get custom content by type
   * 
   * Returns all custom content for a given type. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the custom content, the container of the custom content, and the corresponding space (if different from the container).
   */
  'getCustomContentByType'(
    parameters?: Parameters<Paths.GetCustomContentByType.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentByType.Responses.$200>
  /**
   * createCustomContent - Create custom content
   * 
   * Creates a new custom content in the given space, page, blogpost or other custom content.
   * 
   * Only one of `spaceId`, `pageId`, `blogPostId`, or `customContentId` is required in the request body.
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blogpost and its corresponding space. Permission to create custom content in the space.
   */
  'createCustomContent'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreateCustomContent.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateCustomContent.Responses.$201>
  /**
   * getCustomContentById - Get custom content by id
   * 
   * Returns a specific piece of custom content. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the custom content, the container of the custom content, and the corresponding space (if different from the container).
   */
  'getCustomContentById'(
    parameters?: Parameters<Paths.GetCustomContentById.PathParameters & Paths.GetCustomContentById.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentById.Responses.$200>
  /**
   * updateCustomContent - Update custom content
   * 
   * Update a custom content by id.
   * 
   * `spaceId` is always required and maximum one of `pageId`, `blogPostId`, or `customContentId` is allowed in the request body.
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blogpost and its corresponding space. Permission to update custom content in the space.
   */
  'updateCustomContent'(
    parameters?: Parameters<Paths.UpdateCustomContent.PathParameters> | null,
    data?: Paths.UpdateCustomContent.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateCustomContent.Responses.$200>
  /**
   * deleteCustomContent - Delete custom content
   * 
   * Delete a custom content by id.
   * 
   * Deleting a custom content will either move it to the trash or permanently delete it (purge it), depending on the apiSupport.
   * To permanently delete a **trashed** custom content, the endpoint must be called with the following param `purge=true`.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blogpost and its corresponding space.
   * Permission to delete custom content in the space.
   * Permission to administer the space (if attempting to purge).
   */
  'deleteCustomContent'(
    parameters?: Parameters<Paths.DeleteCustomContent.PathParameters & Paths.DeleteCustomContent.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getCustomContentAttachments - Get attachments for custom content
   * 
   * Returns the attachments of specific custom content. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the custom content and its corresponding space.
   */
  'getCustomContentAttachments'(
    parameters?: Parameters<Paths.GetCustomContentAttachments.PathParameters & Paths.GetCustomContentAttachments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentAttachments.Responses.$200>
  /**
   * getCustomContentComments - Get custom content comments
   * 
   * Returns the comments of the specific custom content.
   * The number of results is limited by the `limit` parameter and additional results (if available) will be available through
   * the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the custom content and its corresponding containers.
   */
  'getCustomContentComments'(
    parameters?: Parameters<Paths.GetCustomContentComments.PathParameters & Paths.GetCustomContentComments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentComments.Responses.$200>
  /**
   * getCustomContentLabels - Get labels for custom content
   * 
   * Returns the labels for a specific piece of custom content. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the custom content and its corresponding space.
   */
  'getCustomContentLabels'(
    parameters?: Parameters<Paths.GetCustomContentLabels.PathParameters & Paths.GetCustomContentLabels.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentLabels.Responses.$200>
  /**
   * getCustomContentOperations - Get permitted operations for custom content
   * 
   * Returns the permitted operations on specific custom content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the parent content of the custom content and its corresponding space.
   */
  'getCustomContentOperations'(
    parameters?: Parameters<Paths.GetCustomContentOperations.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentOperations.Responses.$200>
  /**
   * getCustomContentContentProperties - Get content properties for custom content
   * 
   * Retrieves Content Properties tied to a specified custom content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the custom content.
   */
  'getCustomContentContentProperties'(
    parameters?: Parameters<Paths.GetCustomContentContentProperties.PathParameters & Paths.GetCustomContentContentProperties.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentContentProperties.Responses.$200>
  /**
   * createCustomContentProperty - Create content property for custom content
   * 
   * Creates a new content property for a piece of custom content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the custom content.
   */
  'createCustomContentProperty'(
    parameters?: Parameters<Paths.CreateCustomContentProperty.PathParameters> | null,
    data?: Paths.CreateCustomContentProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateCustomContentProperty.Responses.$200>
  /**
   * getCustomContentContentPropertiesById - Get content property for custom content by id
   * 
   * Retrieves a specific Content Property by ID that is attached to a specified custom content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the page.
   */
  'getCustomContentContentPropertiesById'(
    parameters?: Parameters<Paths.GetCustomContentContentPropertiesById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentContentPropertiesById.Responses.$200>
  /**
   * updateCustomContentPropertyById - Update content property for custom content by id
   * 
   * Update a content property for a piece of custom content by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the custom content.
   */
  'updateCustomContentPropertyById'(
    parameters?: Parameters<Paths.UpdateCustomContentPropertyById.PathParameters> | null,
    data?: Paths.UpdateCustomContentPropertyById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateCustomContentPropertyById.Responses.$200>
  /**
   * deleteCustomContentPropertyById - Delete content property for custom content by id
   * 
   * Deletes a content property for a piece of custom content by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the custom content.
   */
  'deleteCustomContentPropertyById'(
    parameters?: Parameters<Paths.DeleteCustomContentPropertyById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeleteCustomContentPropertyById.Responses.$204>
  /**
   * getLabels - Get labels
   * 
   * Returns all labels. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * Only labels that the user has permission to view will be returned.
   */
  'getLabels'(
    parameters?: Parameters<Paths.GetLabels.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetLabels.Responses.$200>
  /**
   * getLabelAttachments - Get attachments for label
   * 
   * Returns the attachments of specified label. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the attachment and its corresponding space.
   */
  'getLabelAttachments'(
    parameters?: Parameters<Paths.GetLabelAttachments.PathParameters & Paths.GetLabelAttachments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetLabelAttachments.Responses.$200>
  /**
   * getLabelBlogPosts - Get blog posts for label
   * 
   * Returns the blogposts of specified label. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page and its corresponding space.
   */
  'getLabelBlogPosts'(
    parameters?: Parameters<Paths.GetLabelBlogPosts.PathParameters & Paths.GetLabelBlogPosts.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetLabelBlogPosts.Responses.$200>
  /**
   * getLabelPages - Get pages for label
   * 
   * Returns the pages of specified label. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page and its corresponding space.
   */
  'getLabelPages'(
    parameters?: Parameters<Paths.GetLabelPages.PathParameters & Paths.GetLabelPages.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetLabelPages.Responses.$200>
  /**
   * getPages - Get pages
   * 
   * Returns all pages. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * Only pages that the user has permission to view will be returned.
   */
  'getPages'(
    parameters?: Parameters<Paths.GetPages.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPages.Responses.$200>
  /**
   * createPage - Create page
   * 
   * Creates a page in the space.
   * 
   * Pages are created as published by default unless specified as a draft in the status field. If creating a published page, the title must be specified.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the corresponding space. Permission to create a page in the space.
   */
  'createPage'(
    parameters?: Parameters<Paths.CreatePage.QueryParameters> | null,
    data?: Paths.CreatePage.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreatePage.Responses.$200>
  /**
   * getPageById - Get page by id
   * 
   * Returns a specific page.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the page and its corresponding space.
   */
  'getPageById'(
    parameters?: Parameters<Paths.GetPageById.PathParameters & Paths.GetPageById.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageById.Responses.$200>
  /**
   * updatePage - Update page
   * 
   * Update a page by id.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the page and its corresponding space. Permission to update pages in the space.
   */
  'updatePage'(
    parameters?: Parameters<Paths.UpdatePage.PathParameters> | null,
    data?: Paths.UpdatePage.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdatePage.Responses.$200>
  /**
   * deletePage - Delete page
   * 
   * Delete a page by id.
   * 
   * By default this will delete pages that are non-drafts. To delete a page that is a draft, the endpoint must be called on a 
   * draft with the following param `draft=true`. Discarded drafts are not sent to the trash and are permanently deleted.
   * 
   * Deleting a page moves the page to the trash, where it can be restored later. To permanently delete a page (or "purge" it),
   * the endpoint must be called on a **trashed** page with the following param `purge=true`.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the page and its corresponding space.
   * Permission to delete pages in the space.
   * Permission to administer the space (if attempting to purge).
   */
  'deletePage'(
    parameters?: Parameters<Paths.DeletePage.PathParameters & Paths.DeletePage.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getPageAttachments - Get attachments for page
   * 
   * Returns the attachments of specific page. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page and its corresponding space.
   */
  'getPageAttachments'(
    parameters?: Parameters<Paths.GetPageAttachments.PathParameters & Paths.GetPageAttachments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageAttachments.Responses.$200>
  /**
   * getCustomContentByTypeInPage - Get custom content by type in page
   * 
   * Returns all custom content for a given type within a given page. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the custom content, the container of the custom content (page), and the corresponding space.
   */
  'getCustomContentByTypeInPage'(
    parameters?: Parameters<Paths.GetCustomContentByTypeInPage.PathParameters & Paths.GetCustomContentByTypeInPage.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentByTypeInPage.Responses.$200>
  /**
   * getPageLabels - Get labels for page
   * 
   * Returns the labels of specific page. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page and its corresponding space.
   */
  'getPageLabels'(
    parameters?: Parameters<Paths.GetPageLabels.PathParameters & Paths.GetPageLabels.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageLabels.Responses.$200>
  /**
   * getPageLikeCount - Get like count for page
   * 
   * Returns the count of likes of specific page.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page and its corresponding space.
   */
  'getPageLikeCount'(
    parameters?: Parameters<Paths.GetPageLikeCount.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageLikeCount.Responses.$200>
  /**
   * getPageLikeUsers - Get account IDs of likes for page
   * 
   * Returns the account IDs of likes of specific page.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page and its corresponding space.
   */
  'getPageLikeUsers'(
    parameters?: Parameters<Paths.GetPageLikeUsers.PathParameters & Paths.GetPageLikeUsers.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageLikeUsers.Responses.$200>
  /**
   * getPageOperations - Get permitted operations for page
   * 
   * Returns the permitted operations on specific page.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the parent content of the page and its corresponding space.
   */
  'getPageOperations'(
    parameters?: Parameters<Paths.GetPageOperations.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageOperations.Responses.$200>
  /**
   * getPageContentProperties - Get content properties for page
   * 
   * Retrieves Content Properties tied to a specified page.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the page.
   */
  'getPageContentProperties'(
    parameters?: Parameters<Paths.GetPageContentProperties.PathParameters & Paths.GetPageContentProperties.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageContentProperties.Responses.$200>
  /**
   * createPageProperty - Create content property for page
   * 
   * Creates a new content property for a page.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the page.
   */
  'createPageProperty'(
    parameters?: Parameters<Paths.CreatePageProperty.PathParameters> | null,
    data?: Paths.CreatePageProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreatePageProperty.Responses.$200>
  /**
   * getPageContentPropertiesById - Get content property for page by id
   * 
   * Retrieves a specific Content Property by ID that is attached to a specified page.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the page.
   */
  'getPageContentPropertiesById'(
    parameters?: Parameters<Paths.GetPageContentPropertiesById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageContentPropertiesById.Responses.$200>
  /**
   * updatePagePropertyById - Update content property for page by id
   * 
   * Update a content property for a page by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the page.
   */
  'updatePagePropertyById'(
    parameters?: Parameters<Paths.UpdatePagePropertyById.PathParameters> | null,
    data?: Paths.UpdatePagePropertyById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdatePagePropertyById.Responses.$200>
  /**
   * deletePagePropertyById - Delete content property for page by id
   * 
   * Deletes a content property for a page by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the page.
   */
  'deletePagePropertyById'(
    parameters?: Parameters<Paths.DeletePagePropertyById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeletePagePropertyById.Responses.$204>
  /**
   * getPageVersions - Get page versions
   * 
   * Returns the versions of specific page.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the page and its corresponding space.
   */
  'getPageVersions'(
    parameters?: Parameters<Paths.GetPageVersions.PathParameters & Paths.GetPageVersions.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageVersions.Responses.$200>
  /**
   * createWhiteboard - Create whiteboard
   * 
   * Creates a whiteboard in the space.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the corresponding space. Permission to create a whiteboard in the space.
   */
  'createWhiteboard'(
    parameters?: Parameters<Paths.CreateWhiteboard.QueryParameters> | null,
    data?: Paths.CreateWhiteboard.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateWhiteboard.Responses.$200>
  /**
   * getWhiteboardById - Get whiteboard by id
   * 
   * Returns a specific whiteboard.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the whiteboard and its corresponding space.
   */
  'getWhiteboardById'(
    parameters?: Parameters<Paths.GetWhiteboardById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWhiteboardById.Responses.$200>
  /**
   * deleteWhiteboard - Delete whiteboard
   * 
   * Delete a whiteboard by id.
   * 
   * Deleting a whiteboard moves the whiteboard to the trash, where it can be restored later
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the whiteboard and its corresponding space.
   * Permission to delete whiteboards in the space.
   */
  'deleteWhiteboard'(
    parameters?: Parameters<Paths.DeleteWhiteboard.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getWhiteboardContentProperties - Get content properties for whiteboard
   * 
   * Retrieves Content Properties tied to a specified whiteboard.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the whiteboard.
   */
  'getWhiteboardContentProperties'(
    parameters?: Parameters<Paths.GetWhiteboardContentProperties.PathParameters & Paths.GetWhiteboardContentProperties.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWhiteboardContentProperties.Responses.$200>
  /**
   * createWhiteboardProperty - Create content property for whiteboard
   * 
   * Creates a new content property for a whiteboard.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the whiteboard.
   */
  'createWhiteboardProperty'(
    parameters?: Parameters<Paths.CreateWhiteboardProperty.PathParameters> | null,
    data?: Paths.CreateWhiteboardProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateWhiteboardProperty.Responses.$200>
  /**
   * getWhiteboardContentPropertiesById - Get content property for whiteboard by id
   * 
   * Retrieves a specific Content Property by ID that is attached to a specified whiteboard.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the whiteboard.
   */
  'getWhiteboardContentPropertiesById'(
    parameters?: Parameters<Paths.GetWhiteboardContentPropertiesById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWhiteboardContentPropertiesById.Responses.$200>
  /**
   * updateWhiteboardPropertyById - Update content property for whiteboard by id
   * 
   * Update a content property for a whiteboard by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the whiteboard.
   */
  'updateWhiteboardPropertyById'(
    parameters?: Parameters<Paths.UpdateWhiteboardPropertyById.PathParameters> | null,
    data?: Paths.UpdateWhiteboardPropertyById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateWhiteboardPropertyById.Responses.$200>
  /**
   * deleteWhiteboardPropertyById - Delete content property for whiteboard by id
   * 
   * Deletes a content property for a whiteboard by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the whiteboard.
   */
  'deleteWhiteboardPropertyById'(
    parameters?: Parameters<Paths.DeleteWhiteboardPropertyById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeleteWhiteboardPropertyById.Responses.$204>
  /**
   * getWhiteboardOperations - Get permitted operations for a whiteboard
   * 
   * Returns the permitted operations on specific whiteboard.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the whiteboard and its corresponding space.
   */
  'getWhiteboardOperations'(
    parameters?: Parameters<Paths.GetWhiteboardOperations.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWhiteboardOperations.Responses.$200>
  /**
   * getWhiteboardAncestors - Get all ancestors of the whiteboard
   * 
   * Returns all ancestors for a given whiteboard by ID in top-to-bottom order (that is, the highest ancestor is the first
   * item in the response payload). The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available by calling this endpoint with the ID of first ancestor in the response payload.
   * 
   * This endpoint returns minimal information about each ancestor. To fetch more details, use a related endpoint, such
   * as [Get page by id](https://developer.atlassian.com/cloud/confluence/rest/v2/api-group-page/#api-pages-id-get).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * Permission to view the whiteboard and its corresponding space
   */
  'getWhiteboardAncestors'(
    parameters?: Parameters<Paths.GetWhiteboardAncestors.PathParameters & Paths.GetWhiteboardAncestors.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWhiteboardAncestors.Responses.$200>
  /**
   * getPageVersionDetails - Get version details for page version
   * 
   * Retrieves version details for the specified page and version number.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the page.
   */
  'getPageVersionDetails'(
    parameters?: Parameters<Paths.GetPageVersionDetails.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageVersionDetails.Responses.$200>
  /**
   * getCustomContentVersions - Get custom content versions
   * 
   * Returns the versions of specific custom content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the custom content and its corresponding page and space.
   */
  'getCustomContentVersions'(
    parameters?: Parameters<Paths.GetCustomContentVersions.PathParameters & Paths.GetCustomContentVersions.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentVersions.Responses.$200>
  /**
   * getCustomContentVersionDetails - Get version details for custom content version
   * 
   * Retrieves version details for the specified custom content and version number.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the page.
   */
  'getCustomContentVersionDetails'(
    parameters?: Parameters<Paths.GetCustomContentVersionDetails.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentVersionDetails.Responses.$200>
  /**
   * getSpaces - Get spaces
   * 
   * Returns all spaces. The results will be sorted by id ascending. The number of results is limited by the `limit` parameter and
   * additional results (if available) will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * Only spaces that the user has permission to view will be returned.
   */
  'getSpaces'(
    parameters?: Parameters<Paths.GetSpaces.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaces.Responses.$200>
  /**
   * getSpaceById - Get space by id
   * 
   * Returns a specific space.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the space.
   */
  'getSpaceById'(
    parameters?: Parameters<Paths.GetSpaceById.PathParameters & Paths.GetSpaceById.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaceById.Responses.$200>
  /**
   * getBlogPostsInSpace - Get blog posts in space
   * 
   * Returns all blog posts in a space. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission) and view the space.
   * Only blog posts that the user has permission to view will be returned.
   */
  'getBlogPostsInSpace'(
    parameters?: Parameters<Paths.GetBlogPostsInSpace.PathParameters & Paths.GetBlogPostsInSpace.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogPostsInSpace.Responses.$200>
  /**
   * getSpaceLabels - Get labels for space
   * 
   * Returns the labels of specific space. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the space.
   */
  'getSpaceLabels'(
    parameters?: Parameters<Paths.GetSpaceLabels.PathParameters & Paths.GetSpaceLabels.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaceLabels.Responses.$200>
  /**
   * getSpaceContentLabels - Get labels for space content
   * 
   * Returns the labels of space content (pages, blogposts etc). The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the space.
   */
  'getSpaceContentLabels'(
    parameters?: Parameters<Paths.GetSpaceContentLabels.PathParameters & Paths.GetSpaceContentLabels.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaceContentLabels.Responses.$200>
  /**
   * getCustomContentByTypeInSpace - Get custom content by type in space
   * 
   * Returns all custom content for a given type within a given space. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the custom content and the corresponding space.
   */
  'getCustomContentByTypeInSpace'(
    parameters?: Parameters<Paths.GetCustomContentByTypeInSpace.PathParameters & Paths.GetCustomContentByTypeInSpace.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentByTypeInSpace.Responses.$200>
  /**
   * getSpaceOperations - Get permitted operations for space
   * 
   * Returns the permitted operations on specific space.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the corresponding space.
   */
  'getSpaceOperations'(
    parameters?: Parameters<Paths.GetSpaceOperations.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaceOperations.Responses.$200>
  /**
   * getPagesInSpace - Get pages in space
   * 
   * Returns all pages in a space. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission) and 'View' permission for the space.
   * Only pages that the user has permission to view will be returned.
   */
  'getPagesInSpace'(
    parameters?: Parameters<Paths.GetPagesInSpace.PathParameters & Paths.GetPagesInSpace.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPagesInSpace.Responses.$200>
  /**
   * getSpaceProperties - Get space properties in space
   * 
   * Returns all properties for the given space. Space properties are a key-value storage associated with a space.
   * The limit parameter specifies the maximum number of results returned in a single response. Use the `link` response header
   * to paginate through additional results.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission) and 'View' permission for the space.
   */
  'getSpaceProperties'(
    parameters?: Parameters<Paths.GetSpaceProperties.PathParameters & Paths.GetSpaceProperties.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaceProperties.Responses.$200>
  /**
   * createSpaceProperty - Create space property in space
   * 
   * Creates a new space property.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission) and 'Admin' permission for the space.
   */
  'createSpaceProperty'(
    parameters?: Parameters<Paths.CreateSpaceProperty.PathParameters> | null,
    data?: Paths.CreateSpaceProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateSpaceProperty.Responses.$201>
  /**
   * getSpacePropertyById - Get space property by id
   * 
   * Retrieve a space property by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission) and 'View' permission for the space.
   */
  'getSpacePropertyById'(
    parameters?: Parameters<Paths.GetSpacePropertyById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpacePropertyById.Responses.$200>
  /**
   * updateSpacePropertyById - Update space property by id
   * 
   * Update a space property by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission) and 'Admin' permission for the space.
   */
  'updateSpacePropertyById'(
    parameters?: Parameters<Paths.UpdateSpacePropertyById.PathParameters> | null,
    data?: Paths.UpdateSpacePropertyById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateSpacePropertyById.Responses.$200>
  /**
   * deleteSpacePropertyById - Delete space property by id
   * 
   * Deletes a space property by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission) and 'Admin' permission for the space.
   */
  'deleteSpacePropertyById'(
    parameters?: Parameters<Paths.DeleteSpacePropertyById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeleteSpacePropertyById.Responses.$204>
  /**
   * getSpacePermissions - Get space permissions
   * 
   * Returns space permissions for a specific space.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the space.
   */
  'getSpacePermissions'(
    parameters?: Parameters<Paths.GetSpacePermissions.PathParameters & Paths.GetSpacePermissions.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpacePermissions.Responses.$200>
  /**
   * getPageFooterComments - Get footer comments for page
   * 
   * Returns the root footer comments of specific page. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page and its corresponding space.
   */
  'getPageFooterComments'(
    parameters?: Parameters<Paths.GetPageFooterComments.PathParameters & Paths.GetPageFooterComments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageFooterComments.Responses.$200>
  /**
   * getPageInlineComments - Get inline comments for page
   * 
   * Returns the root inline comments of specific page. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page and its corresponding space.
   */
  'getPageInlineComments'(
    parameters?: Parameters<Paths.GetPageInlineComments.PathParameters & Paths.GetPageInlineComments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageInlineComments.Responses.$200>
  /**
   * getBlogPostFooterComments - Get footer comments for blog post
   * 
   * Returns the root footer comments of specific blog post. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the blog post and its corresponding space.
   */
  'getBlogPostFooterComments'(
    parameters?: Parameters<Paths.GetBlogPostFooterComments.PathParameters & Paths.GetBlogPostFooterComments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogPostFooterComments.Responses.$200>
  /**
   * getBlogPostInlineComments - Get inline comments for blog post
   * 
   * Returns the root inline comments of specific blog post. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the blog post and its corresponding space.
   */
  'getBlogPostInlineComments'(
    parameters?: Parameters<Paths.GetBlogPostInlineComments.PathParameters & Paths.GetBlogPostInlineComments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlogPostInlineComments.Responses.$200>
  /**
   * getFooterComments - Get footer comments
   * 
   * Returns all footer comments. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the container and its corresponding space.
   */
  'getFooterComments'(
    parameters?: Parameters<Paths.GetFooterComments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetFooterComments.Responses.$200>
  /**
   * createFooterComment - Create footer comment
   * 
   * Create a footer comment.
   * 
   * The footer comment can be made against several locations: 
   * - at the top level (specifying pageId or blogPostId in the request body)
   * - as a reply (specifying parentCommentId in the request body)
   * - against an attachment (note: this is different than the comments added via the attachment properties page on the UI, which are referred to as version comments)
   * - against a custom content
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blogpost and its corresponding space. Permission to create comments in the space.
   */
  'createFooterComment'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreateFooterComment.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateFooterComment.Responses.$201>
  /**
   * getFooterCommentById - Get footer comment by id
   * 
   * Retrieves a footer comment by id
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the container and its corresponding space.
   */
  'getFooterCommentById'(
    parameters?: Parameters<Paths.GetFooterCommentById.PathParameters & Paths.GetFooterCommentById.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetFooterCommentById.Responses.$200>
  /**
   * updateFooterComment - Update footer comment
   * 
   * Update a footer comment. This can be used to update the body text of a comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blogpost and its corresponding space. Permission to create comments in the space.
   */
  'updateFooterComment'(
    parameters?: Parameters<Paths.UpdateFooterComment.PathParameters> | null,
    data?: Paths.UpdateFooterComment.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateFooterComment.Responses.$200>
  /**
   * deleteFooterComment - Delete footer comment
   * 
   * Deletes a footer comment. This is a permanent deletion and cannot be reverted.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blogpost and its corresponding space. Permission to delete comments in the space.
   */
  'deleteFooterComment'(
    parameters?: Parameters<Paths.DeleteFooterComment.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeleteFooterComment.Responses.$204>
  /**
   * getFooterCommentChildren - Get children footer comments
   * 
   * Returns the children footer comments of specific comment. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page and its corresponding space.
   */
  'getFooterCommentChildren'(
    parameters?: Parameters<Paths.GetFooterCommentChildren.PathParameters & Paths.GetFooterCommentChildren.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetFooterCommentChildren.Responses.$200>
  /**
   * getFooterLikeCount - Get like count for footer comment
   * 
   * Returns the count of likes of specific footer comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page/blogpost and its corresponding space.
   */
  'getFooterLikeCount'(
    parameters?: Parameters<Paths.GetFooterLikeCount.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetFooterLikeCount.Responses.$200>
  /**
   * getFooterLikeUsers - Get account IDs of likes for footer comment
   * 
   * Returns the account IDs of likes of specific footer comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page/blogpost and its corresponding space.
   */
  'getFooterLikeUsers'(
    parameters?: Parameters<Paths.GetFooterLikeUsers.PathParameters & Paths.GetFooterLikeUsers.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetFooterLikeUsers.Responses.$200>
  /**
   * getFooterCommentOperations - Get permitted operations for footer comment
   * 
   * Returns the permitted operations on specific footer comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the parent content of the footer comment and its corresponding space.
   */
  'getFooterCommentOperations'(
    parameters?: Parameters<Paths.GetFooterCommentOperations.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetFooterCommentOperations.Responses.$200>
  /**
   * getFooterCommentVersions - Get footer comment versions
   * 
   * Retrieves the versions of the specified footer comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blog post and its corresponding space.
   */
  'getFooterCommentVersions'(
    parameters?: Parameters<Paths.GetFooterCommentVersions.PathParameters & Paths.GetFooterCommentVersions.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetFooterCommentVersions.Responses.$200>
  /**
   * getFooterCommentVersionDetails - Get version details for footer comment version
   * 
   * Retrieves version details for the specified footer comment version.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blog post and its corresponding space.
   */
  'getFooterCommentVersionDetails'(
    parameters?: Parameters<Paths.GetFooterCommentVersionDetails.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetFooterCommentVersionDetails.Responses.$200>
  /**
   * getInlineComments - Get inline comments
   * 
   * Returns all inline comments. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page and its corresponding space.
   */
  'getInlineComments'(
    parameters?: Parameters<Paths.GetInlineComments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetInlineComments.Responses.$200>
  /**
   * createInlineComment - Create inline comment
   * 
   * Create an inline comment. This can be at the top level (specifying pageId or blogPostId in the request body)
   * or as a reply (specifying parentCommentId in the request body). Note the inlineCommentProperties object in the
   * request body is used to select the text the inline comment should be tied to. This is what determines the text 
   * highlighting when viewing a page in Confluence.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blogpost and its corresponding space. Permission to create comments in the space.
   */
  'createInlineComment'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreateInlineComment.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateInlineComment.Responses.$201>
  /**
   * getInlineCommentById - Get inline comment by id
   * 
   * Retrieves an inline comment by id
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blogpost and its corresponding space.
   */
  'getInlineCommentById'(
    parameters?: Parameters<Paths.GetInlineCommentById.PathParameters & Paths.GetInlineCommentById.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetInlineCommentById.Responses.$200>
  /**
   * updateInlineComment - Update inline comment
   * 
   * Update an inline comment. This can be used to update the body text of a comment and/or to resolve the comment
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blogpost and its corresponding space. Permission to create comments in the space.
   */
  'updateInlineComment'(
    parameters?: Parameters<Paths.UpdateInlineComment.PathParameters> | null,
    data?: Paths.UpdateInlineComment.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateInlineComment.Responses.$200>
  /**
   * deleteInlineComment - Delete inline comment
   * 
   * Deletes an inline comment. This is a permanent deletion and cannot be reverted.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blogpost and its corresponding space. Permission to delete comments in the space.
   */
  'deleteInlineComment'(
    parameters?: Parameters<Paths.DeleteInlineComment.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeleteInlineComment.Responses.$204>
  /**
   * getInlineCommentChildren - Get children inline comments
   * 
   * Returns the children inline comments of specific comment. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page and its corresponding space.
   */
  'getInlineCommentChildren'(
    parameters?: Parameters<Paths.GetInlineCommentChildren.PathParameters & Paths.GetInlineCommentChildren.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetInlineCommentChildren.Responses.$200>
  /**
   * getInlineLikeCount - Get like count for inline comment
   * 
   * Returns the count of likes of specific inline comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page/blogpost and its corresponding space.
   */
  'getInlineLikeCount'(
    parameters?: Parameters<Paths.GetInlineLikeCount.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetInlineLikeCount.Responses.$200>
  /**
   * getInlineLikeUsers - Get account IDs of likes for inline comment
   * 
   * Returns the account IDs of likes of specific inline comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page/blogpost and its corresponding space.
   */
  'getInlineLikeUsers'(
    parameters?: Parameters<Paths.GetInlineLikeUsers.PathParameters & Paths.GetInlineLikeUsers.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetInlineLikeUsers.Responses.$200>
  /**
   * getInlineCommentOperations - Get permitted operations for inline comment
   * 
   * Returns the permitted operations on specific inline comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the parent content of the inline comment and its corresponding space.
   */
  'getInlineCommentOperations'(
    parameters?: Parameters<Paths.GetInlineCommentOperations.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetInlineCommentOperations.Responses.$200>
  /**
   * getInlineCommentVersions - Get inline comment versions
   * 
   * Retrieves the versions of the specified inline comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blog post and its corresponding space.
   */
  'getInlineCommentVersions'(
    parameters?: Parameters<Paths.GetInlineCommentVersions.PathParameters & Paths.GetInlineCommentVersions.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetInlineCommentVersions.Responses.$200>
  /**
   * getInlineCommentVersionDetails - Get version details for inline comment version
   * 
   * Retrieves version details for the specified inline comment version.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content of the page or blog post and its corresponding space.
   */
  'getInlineCommentVersionDetails'(
    parameters?: Parameters<Paths.GetInlineCommentVersionDetails.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetInlineCommentVersionDetails.Responses.$200>
  /**
   * getCommentContentProperties - Get content properties for comment
   * 
   * Retrieves Content Properties attached to a specified comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the comment.
   */
  'getCommentContentProperties'(
    parameters?: Parameters<Paths.GetCommentContentProperties.PathParameters & Paths.GetCommentContentProperties.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCommentContentProperties.Responses.$200>
  /**
   * createCommentProperty - Create content property for comment
   * 
   * Creates a new content property for a comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the comment.
   */
  'createCommentProperty'(
    parameters?: Parameters<Paths.CreateCommentProperty.PathParameters> | null,
    data?: Paths.CreateCommentProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateCommentProperty.Responses.$200>
  /**
   * getCommentContentPropertiesById - Get content property for comment by id
   * 
   * Retrieves a specific Content Property by ID that is attached to a specified comment.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the comment.
   */
  'getCommentContentPropertiesById'(
    parameters?: Parameters<Paths.GetCommentContentPropertiesById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCommentContentPropertiesById.Responses.$200>
  /**
   * updateCommentPropertyById - Update content property for comment by id
   * 
   * Update a content property for a comment by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the comment.
   */
  'updateCommentPropertyById'(
    parameters?: Parameters<Paths.UpdateCommentPropertyById.PathParameters> | null,
    data?: Paths.UpdateCommentPropertyById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateCommentPropertyById.Responses.$200>
  /**
   * deleteCommentPropertyById - Delete content property for comment by id
   * 
   * Deletes a content property for a comment by its id. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the comment.
   */
  'deleteCommentPropertyById'(
    parameters?: Parameters<Paths.DeleteCommentPropertyById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeleteCommentPropertyById.Responses.$204>
  /**
   * getTasks - Get tasks
   * 
   * Returns all tasks. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * Only tasks that the user has permission to view will be returned.
   */
  'getTasks'(
    parameters?: Parameters<Paths.GetTasks.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetTasks.Responses.$200>
  /**
   * getTaskById - Get task by id
   * 
   * Returns a specific task. 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the containing page or blog post and its corresponding space.
   */
  'getTaskById'(
    parameters?: Parameters<Paths.GetTaskById.PathParameters & Paths.GetTaskById.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetTaskById.Responses.$200>
  /**
   * updateTask - Update task
   * 
   * Update a task by id.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the containing page or blog post and view its corresponding space.
   */
  'updateTask'(
    parameters?: Parameters<Paths.UpdateTask.PathParameters & Paths.UpdateTask.QueryParameters> | null,
    data?: Paths.UpdateTask.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateTask.Responses.$200>
  /**
   * getChildPages - Get child pages
   * 
   * Returns all child pages for given page id. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * Only pages that the user has permission to view will be returned.
   */
  'getChildPages'(
    parameters?: Parameters<Paths.GetChildPages.PathParameters & Paths.GetChildPages.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetChildPages.Responses.$200>
  /**
   * getChildCustomContent - Get child custom content
   * 
   * Returns all child custom content for given custom content id. The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * Only custom content that the user has permission to view will be returned.
   */
  'getChildCustomContent'(
    parameters?: Parameters<Paths.GetChildCustomContent.PathParameters & Paths.GetChildCustomContent.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetChildCustomContent.Responses.$200>
  /**
   * getPageAncestors - Get all ancestors of page
   * 
   * Returns all ancestors for a given page by ID in top-to-bottom order (that is, the highest ancestor is the first
   * item in the response payload). The number of results is limited by the `limit` parameter and additional results (if available)
   * will be available by calling this endpoint with the ID of first ancestor in the response payload.
   * 
   * This endpoint returns minimal information about each ancestor. To fetch more details, use a related endpoint, such
   * as [Get page by id](https://developer.atlassian.com/cloud/confluence/rest/v2/api-group-page/#api-pages-id-get).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getPageAncestors'(
    parameters?: Parameters<Paths.GetPageAncestors.PathParameters & Paths.GetPageAncestors.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPageAncestors.Responses.$200>
  /**
   * checkAccessByEmail - Check site access for a list of emails
   * 
   * Returns the list of emails from the input list that do not have access to site.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'checkAccessByEmail'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CheckAccessByEmail.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CheckAccessByEmail.Responses.$200>
  /**
   * inviteByEmail - Invite a list of emails to the site
   * 
   * Invite a list of emails to the site.
   * 
   * Ignores all invalid emails and no action is taken for the emails that already have access to the site.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'inviteByEmail'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.InviteByEmail.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getDataPolicyMetadata - Get data policy metadata for the workspace (EAP)
   * 
   * Returns data policy metadata for the workspace.
   * 
   * **[Permissions](#permissions) required:**
   * Only apps can make this request.
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getDataPolicyMetadata'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetDataPolicyMetadata.Responses.$200>
  /**
   * getDataPolicySpaces - Get spaces with data policies (EAP)
   * 
   * Returns all spaces. The results will be sorted by id ascending. The number of results is limited by the `limit` parameter and
   * additional results (if available) will be available through the `next` URL present in the `Link` response header.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Only apps can make this request.
   * Permission to access the Confluence site ('Can use' global permission).
   * Only spaces that the app has permission to view will be returned.
   */
  'getDataPolicySpaces'(
    parameters?: Parameters<Paths.GetDataPolicySpaces.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetDataPolicySpaces.Responses.$200>
}

export interface PathsDictionary {
  ['/attachments']: {
    /**
     * getAttachments - Get attachments
     * 
     * Returns all attachments. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the container of the attachment.
     */
    'get'(
      parameters?: Parameters<Paths.GetAttachments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAttachments.Responses.$200>
  }
  ['/attachments/{id}']: {
    /**
     * getAttachmentById - Get attachment by id
     * 
     * Returns a specific attachment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the attachment's container.
     */
    'get'(
      parameters?: Parameters<Paths.GetAttachmentById.PathParameters & Paths.GetAttachmentById.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAttachmentById.Responses.$200>
    /**
     * deleteAttachment - Delete attachment
     * 
     * Delete an attachment by id.
     * 
     * Deleting an attachment moves the attachment to the trash, where it can be restored later. To permanently delete an attachment (or "purge" it),
     * the endpoint must be called on a **trashed** attachment with the following param `purge=true`.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the container of the attachment.
     * Permission to delete attachments in the space.
     * Permission to administer the space (if attempting to purge).
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteAttachment.PathParameters & Paths.DeleteAttachment.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/attachments/{id}/labels']: {
    /**
     * getAttachmentLabels - Get labels for attachment
     * 
     * Returns the labels of specific attachment. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the parent content of the attachment and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetAttachmentLabels.PathParameters & Paths.GetAttachmentLabels.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAttachmentLabels.Responses.$200>
  }
  ['/attachments/{id}/operations']: {
    /**
     * getAttachmentOperations - Get permitted operations for attachment
     * 
     * Returns the permitted operations on specific attachment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the parent content of the attachment and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetAttachmentOperations.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAttachmentOperations.Responses.$200>
  }
  ['/attachments/{attachment-id}/properties']: {
    /**
     * getAttachmentContentProperties - Get content properties for attachment
     * 
     * Retrieves all Content Properties tied to a specified attachment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the attachment.
     */
    'get'(
      parameters?: Parameters<Paths.GetAttachmentContentProperties.PathParameters & Paths.GetAttachmentContentProperties.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAttachmentContentProperties.Responses.$200>
    /**
     * createAttachmentProperty - Create content property for attachment
     * 
     * Creates a new content property for an attachment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the attachment.
     */
    'post'(
      parameters?: Parameters<Paths.CreateAttachmentProperty.PathParameters> | null,
      data?: Paths.CreateAttachmentProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateAttachmentProperty.Responses.$200>
  }
  ['/attachments/{attachment-id}/properties/{property-id}']: {
    /**
     * getAttachmentContentPropertiesById - Get content property for attachment by id
     * 
     * Retrieves a specific Content Property by ID that is attached to a specified attachment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the attachment.
     */
    'get'(
      parameters?: Parameters<Paths.GetAttachmentContentPropertiesById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAttachmentContentPropertiesById.Responses.$200>
    /**
     * updateAttachmentPropertyById - Update content property for attachment by id
     * 
     * Update a content property for attachment by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the attachment.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateAttachmentPropertyById.PathParameters> | null,
      data?: Paths.UpdateAttachmentPropertyById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateAttachmentPropertyById.Responses.$200>
    /**
     * deleteAttachmentPropertyById - Delete content property for attachment by id
     * 
     * Deletes a content property for an attachment by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to attachment the page.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteAttachmentPropertyById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeleteAttachmentPropertyById.Responses.$204>
  }
  ['/attachments/{id}/versions']: {
    /**
     * getAttachmentVersions - Get attachment versions
     * 
     * Returns the versions of specific attachment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the attachment and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetAttachmentVersions.PathParameters & Paths.GetAttachmentVersions.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAttachmentVersions.Responses.$200>
  }
  ['/attachments/{attachment-id}/versions/{version-number}']: {
    /**
     * getAttachmentVersionDetails - Get version details for attachment version
     * 
     * Retrieves version details for the specified attachment and version number.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the attachment.
     */
    'get'(
      parameters?: Parameters<Paths.GetAttachmentVersionDetails.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAttachmentVersionDetails.Responses.$200>
  }
  ['/attachments/{id}/footer-comments']: {
    /**
     * getAttachmentComments - Get attachment comments
     * 
     * Returns the comments of the specific attachment.
     * The number of results is limited by the `limit` parameter and additional results (if available) will be available through
     * the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the attachment and its corresponding containers.
     */
    'get'(
      parameters?: Parameters<Paths.GetAttachmentComments.PathParameters & Paths.GetAttachmentComments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAttachmentComments.Responses.$200>
  }
  ['/blogposts']: {
    /**
     * getBlogPosts - Get blog posts
     * 
     * Returns all blog posts. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * Only blog posts that the user has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogPosts.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogPosts.Responses.$200>
    /**
     * createBlogPost - Create blog post
     * 
     * Creates a new blog post in the space specified by the spaceId.
     * 
     * By default this will create the blog post as a non-draft, unless the status is specified as draft.
     * If creating a non-draft, the title must not be empty.
     * 
     * Currently only supports the storage representation specified in the body.representation enums below
     */
    'post'(
      parameters?: Parameters<Paths.CreateBlogPost.QueryParameters> | null,
      data?: Paths.CreateBlogPost.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateBlogPost.Responses.$200>
  }
  ['/blogposts/{id}']: {
    /**
     * getBlogPostById - Get blog post by id
     * 
     * Returns a specific blog post.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogPostById.PathParameters & Paths.GetBlogPostById.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogPostById.Responses.$200>
    /**
     * updateBlogPost - Update blog post
     * 
     * Update a blog post by id.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the blog post and its corresponding space. Permission to update blog posts in the space.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateBlogPost.PathParameters> | null,
      data?: Paths.UpdateBlogPost.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateBlogPost.Responses.$200>
    /**
     * deleteBlogPost - Delete blog post
     * 
     * Delete a blog post by id.
     * 
     * By default this will delete blog posts that are non-drafts. To delete a blog post that is a draft, the endpoint must be called on a 
     * draft with the following param `draft=true`. Discarded drafts are not sent to the trash and are permanently deleted.
     * 
     * Deleting a blog post that is not a draft moves the blog post to the trash, where it can be restored later. 
     * To permanently delete a blog post (or "purge" it), the endpoint must be called on a **trashed** blog post with the following param `purge=true`.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the blog post and its corresponding space.
     * Permission to delete blog posts in the space.
     * Permission to administer the space (if attempting to purge).
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteBlogPost.PathParameters & Paths.DeleteBlogPost.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/blogposts/{id}/attachments']: {
    /**
     * getBlogpostAttachments - Get attachments for blog post
     * 
     * Returns the attachments of specific blog post. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogpostAttachments.PathParameters & Paths.GetBlogpostAttachments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogpostAttachments.Responses.$200>
  }
  ['/blogposts/{id}/custom-content']: {
    /**
     * getCustomContentByTypeInBlogPost - Get custom content by type in blog post
     * 
     * Returns all custom content for a given type within a given blogpost. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the custom content, the container of the custom content (blog post), and the corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentByTypeInBlogPost.PathParameters & Paths.GetCustomContentByTypeInBlogPost.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentByTypeInBlogPost.Responses.$200>
  }
  ['/blogposts/{id}/labels']: {
    /**
     * getBlogPostLabels - Get labels for blog post
     * 
     * Returns the labels of specific blog post. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogPostLabels.PathParameters & Paths.GetBlogPostLabels.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogPostLabels.Responses.$200>
  }
  ['/blogposts/{id}/likes/count']: {
    /**
     * getBlogPostLikeCount - Get like count for blog post
     * 
     * Returns the count of likes of specific blog post.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogPostLikeCount.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogPostLikeCount.Responses.$200>
  }
  ['/blogposts/{id}/likes/users']: {
    /**
     * getBlogPostLikeUsers - Get account IDs of likes for blog post
     * 
     * Returns the account IDs of likes of specific blog post.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogPostLikeUsers.PathParameters & Paths.GetBlogPostLikeUsers.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogPostLikeUsers.Responses.$200>
  }
  ['/blogposts/{blogpost-id}/properties']: {
    /**
     * getBlogpostContentProperties - Get content properties for blog post
     * 
     * Retrieves all Content Properties tied to a specified blog post.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the blog post.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogpostContentProperties.PathParameters & Paths.GetBlogpostContentProperties.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogpostContentProperties.Responses.$200>
    /**
     * createBlogpostProperty - Create content property for blog post
     * 
     * Creates a new property for a blogpost.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the blog post.
     */
    'post'(
      parameters?: Parameters<Paths.CreateBlogpostProperty.PathParameters> | null,
      data?: Paths.CreateBlogpostProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateBlogpostProperty.Responses.$200>
  }
  ['/blogposts/{blogpost-id}/properties/{property-id}']: {
    /**
     * getBlogpostContentPropertiesById - Get content property for blog post by id
     * 
     * Retrieves a specific Content Property by ID that is attached to a specified blog post.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the blog post.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogpostContentPropertiesById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogpostContentPropertiesById.Responses.$200>
    /**
     * updateBlogpostPropertyById - Update content property for blog post by id
     * 
     * Update a content property for blog post by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the blog post.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateBlogpostPropertyById.PathParameters> | null,
      data?: Paths.UpdateBlogpostPropertyById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateBlogpostPropertyById.Responses.$200>
    /**
     * deleteBlogpostPropertyById - Delete content property for blogpost by id
     * 
     * Deletes a content property for a blogpost by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the blog post.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteBlogpostPropertyById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeleteBlogpostPropertyById.Responses.$204>
  }
  ['/blogposts/{id}/operations']: {
    /**
     * getBlogPostOperations - Get permitted operations for blog post
     * 
     * Returns the permitted operations on specific blog post.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the parent content of the blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogPostOperations.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogPostOperations.Responses.$200>
  }
  ['/blogposts/{id}/versions']: {
    /**
     * getBlogPostVersions - Get blog post versions
     * 
     * Returns the versions of specific blog post. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogPostVersions.PathParameters & Paths.GetBlogPostVersions.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogPostVersions.Responses.$200>
  }
  ['/blogposts/{blogpost-id}/versions/{version-number}']: {
    /**
     * getBlogPostVersionDetails - Get version details for blog post version
     * 
     * Retrieves version details for the specified blog post and version number.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the blog post.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogPostVersionDetails.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogPostVersionDetails.Responses.$200>
  }
  ['/content/convert-ids-to-types']: {
    /**
     * convertContentIdsToContentTypes - Convert content ids to content types
     * 
     * Converts a list of content ids into their associated content types. This is useful for users migrating from v1 to v2
     * who may have stored just content ids without their associated type. This will return types as they should be used in v2.
     * Notably, this will return `inline-comment` for inline comments and `footer-comment` for footer comments, which is distinct from them
     * both being represented by `comment` in v1.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the requested content. Any content that the user does not have permission to view or does not exist will map to `null` in the response.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.ConvertContentIdsToContentTypes.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.ConvertContentIdsToContentTypes.Responses.$200>
  }
  ['/custom-content']: {
    /**
     * getCustomContentByType - Get custom content by type
     * 
     * Returns all custom content for a given type. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the custom content, the container of the custom content, and the corresponding space (if different from the container).
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentByType.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentByType.Responses.$200>
    /**
     * createCustomContent - Create custom content
     * 
     * Creates a new custom content in the given space, page, blogpost or other custom content.
     * 
     * Only one of `spaceId`, `pageId`, `blogPostId`, or `customContentId` is required in the request body.
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blogpost and its corresponding space. Permission to create custom content in the space.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreateCustomContent.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateCustomContent.Responses.$201>
  }
  ['/custom-content/{id}']: {
    /**
     * getCustomContentById - Get custom content by id
     * 
     * Returns a specific piece of custom content. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the custom content, the container of the custom content, and the corresponding space (if different from the container).
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentById.PathParameters & Paths.GetCustomContentById.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentById.Responses.$200>
    /**
     * updateCustomContent - Update custom content
     * 
     * Update a custom content by id.
     * 
     * `spaceId` is always required and maximum one of `pageId`, `blogPostId`, or `customContentId` is allowed in the request body.
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blogpost and its corresponding space. Permission to update custom content in the space.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateCustomContent.PathParameters> | null,
      data?: Paths.UpdateCustomContent.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateCustomContent.Responses.$200>
    /**
     * deleteCustomContent - Delete custom content
     * 
     * Delete a custom content by id.
     * 
     * Deleting a custom content will either move it to the trash or permanently delete it (purge it), depending on the apiSupport.
     * To permanently delete a **trashed** custom content, the endpoint must be called with the following param `purge=true`.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blogpost and its corresponding space.
     * Permission to delete custom content in the space.
     * Permission to administer the space (if attempting to purge).
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteCustomContent.PathParameters & Paths.DeleteCustomContent.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/custom-content/{id}/attachments']: {
    /**
     * getCustomContentAttachments - Get attachments for custom content
     * 
     * Returns the attachments of specific custom content. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the custom content and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentAttachments.PathParameters & Paths.GetCustomContentAttachments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentAttachments.Responses.$200>
  }
  ['/custom-content/{id}/footer-comments']: {
    /**
     * getCustomContentComments - Get custom content comments
     * 
     * Returns the comments of the specific custom content.
     * The number of results is limited by the `limit` parameter and additional results (if available) will be available through
     * the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the custom content and its corresponding containers.
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentComments.PathParameters & Paths.GetCustomContentComments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentComments.Responses.$200>
  }
  ['/custom-content/{id}/labels']: {
    /**
     * getCustomContentLabels - Get labels for custom content
     * 
     * Returns the labels for a specific piece of custom content. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the custom content and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentLabels.PathParameters & Paths.GetCustomContentLabels.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentLabels.Responses.$200>
  }
  ['/custom-content/{id}/operations']: {
    /**
     * getCustomContentOperations - Get permitted operations for custom content
     * 
     * Returns the permitted operations on specific custom content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the parent content of the custom content and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentOperations.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentOperations.Responses.$200>
  }
  ['/custom-content/{custom-content-id}/properties']: {
    /**
     * getCustomContentContentProperties - Get content properties for custom content
     * 
     * Retrieves Content Properties tied to a specified custom content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the custom content.
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentContentProperties.PathParameters & Paths.GetCustomContentContentProperties.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentContentProperties.Responses.$200>
    /**
     * createCustomContentProperty - Create content property for custom content
     * 
     * Creates a new content property for a piece of custom content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the custom content.
     */
    'post'(
      parameters?: Parameters<Paths.CreateCustomContentProperty.PathParameters> | null,
      data?: Paths.CreateCustomContentProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateCustomContentProperty.Responses.$200>
  }
  ['/custom-content/{custom-content-id}/properties/{property-id}']: {
    /**
     * getCustomContentContentPropertiesById - Get content property for custom content by id
     * 
     * Retrieves a specific Content Property by ID that is attached to a specified custom content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the page.
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentContentPropertiesById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentContentPropertiesById.Responses.$200>
    /**
     * updateCustomContentPropertyById - Update content property for custom content by id
     * 
     * Update a content property for a piece of custom content by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the custom content.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateCustomContentPropertyById.PathParameters> | null,
      data?: Paths.UpdateCustomContentPropertyById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateCustomContentPropertyById.Responses.$200>
    /**
     * deleteCustomContentPropertyById - Delete content property for custom content by id
     * 
     * Deletes a content property for a piece of custom content by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the custom content.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteCustomContentPropertyById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeleteCustomContentPropertyById.Responses.$204>
  }
  ['/labels']: {
    /**
     * getLabels - Get labels
     * 
     * Returns all labels. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * Only labels that the user has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetLabels.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetLabels.Responses.$200>
  }
  ['/labels/{id}/attachments']: {
    /**
     * getLabelAttachments - Get attachments for label
     * 
     * Returns the attachments of specified label. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the attachment and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetLabelAttachments.PathParameters & Paths.GetLabelAttachments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetLabelAttachments.Responses.$200>
  }
  ['/labels/{id}/blogposts']: {
    /**
     * getLabelBlogPosts - Get blog posts for label
     * 
     * Returns the blogposts of specified label. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetLabelBlogPosts.PathParameters & Paths.GetLabelBlogPosts.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetLabelBlogPosts.Responses.$200>
  }
  ['/labels/{id}/pages']: {
    /**
     * getLabelPages - Get pages for label
     * 
     * Returns the pages of specified label. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetLabelPages.PathParameters & Paths.GetLabelPages.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetLabelPages.Responses.$200>
  }
  ['/pages']: {
    /**
     * getPages - Get pages
     * 
     * Returns all pages. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * Only pages that the user has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetPages.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPages.Responses.$200>
    /**
     * createPage - Create page
     * 
     * Creates a page in the space.
     * 
     * Pages are created as published by default unless specified as a draft in the status field. If creating a published page, the title must be specified.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the corresponding space. Permission to create a page in the space.
     */
    'post'(
      parameters?: Parameters<Paths.CreatePage.QueryParameters> | null,
      data?: Paths.CreatePage.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreatePage.Responses.$200>
  }
  ['/pages/{id}']: {
    /**
     * getPageById - Get page by id
     * 
     * Returns a specific page.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageById.PathParameters & Paths.GetPageById.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageById.Responses.$200>
    /**
     * updatePage - Update page
     * 
     * Update a page by id.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the page and its corresponding space. Permission to update pages in the space.
     */
    'put'(
      parameters?: Parameters<Paths.UpdatePage.PathParameters> | null,
      data?: Paths.UpdatePage.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdatePage.Responses.$200>
    /**
     * deletePage - Delete page
     * 
     * Delete a page by id.
     * 
     * By default this will delete pages that are non-drafts. To delete a page that is a draft, the endpoint must be called on a 
     * draft with the following param `draft=true`. Discarded drafts are not sent to the trash and are permanently deleted.
     * 
     * Deleting a page moves the page to the trash, where it can be restored later. To permanently delete a page (or "purge" it),
     * the endpoint must be called on a **trashed** page with the following param `purge=true`.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the page and its corresponding space.
     * Permission to delete pages in the space.
     * Permission to administer the space (if attempting to purge).
     */
    'delete'(
      parameters?: Parameters<Paths.DeletePage.PathParameters & Paths.DeletePage.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/pages/{id}/attachments']: {
    /**
     * getPageAttachments - Get attachments for page
     * 
     * Returns the attachments of specific page. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageAttachments.PathParameters & Paths.GetPageAttachments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageAttachments.Responses.$200>
  }
  ['/pages/{id}/custom-content']: {
    /**
     * getCustomContentByTypeInPage - Get custom content by type in page
     * 
     * Returns all custom content for a given type within a given page. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the custom content, the container of the custom content (page), and the corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentByTypeInPage.PathParameters & Paths.GetCustomContentByTypeInPage.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentByTypeInPage.Responses.$200>
  }
  ['/pages/{id}/labels']: {
    /**
     * getPageLabels - Get labels for page
     * 
     * Returns the labels of specific page. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageLabels.PathParameters & Paths.GetPageLabels.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageLabels.Responses.$200>
  }
  ['/pages/{id}/likes/count']: {
    /**
     * getPageLikeCount - Get like count for page
     * 
     * Returns the count of likes of specific page.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageLikeCount.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageLikeCount.Responses.$200>
  }
  ['/pages/{id}/likes/users']: {
    /**
     * getPageLikeUsers - Get account IDs of likes for page
     * 
     * Returns the account IDs of likes of specific page.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageLikeUsers.PathParameters & Paths.GetPageLikeUsers.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageLikeUsers.Responses.$200>
  }
  ['/pages/{id}/operations']: {
    /**
     * getPageOperations - Get permitted operations for page
     * 
     * Returns the permitted operations on specific page.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the parent content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageOperations.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageOperations.Responses.$200>
  }
  ['/pages/{page-id}/properties']: {
    /**
     * getPageContentProperties - Get content properties for page
     * 
     * Retrieves Content Properties tied to a specified page.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the page.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageContentProperties.PathParameters & Paths.GetPageContentProperties.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageContentProperties.Responses.$200>
    /**
     * createPageProperty - Create content property for page
     * 
     * Creates a new content property for a page.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the page.
     */
    'post'(
      parameters?: Parameters<Paths.CreatePageProperty.PathParameters> | null,
      data?: Paths.CreatePageProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreatePageProperty.Responses.$200>
  }
  ['/pages/{page-id}/properties/{property-id}']: {
    /**
     * getPageContentPropertiesById - Get content property for page by id
     * 
     * Retrieves a specific Content Property by ID that is attached to a specified page.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the page.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageContentPropertiesById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageContentPropertiesById.Responses.$200>
    /**
     * updatePagePropertyById - Update content property for page by id
     * 
     * Update a content property for a page by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the page.
     */
    'put'(
      parameters?: Parameters<Paths.UpdatePagePropertyById.PathParameters> | null,
      data?: Paths.UpdatePagePropertyById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdatePagePropertyById.Responses.$200>
    /**
     * deletePagePropertyById - Delete content property for page by id
     * 
     * Deletes a content property for a page by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the page.
     */
    'delete'(
      parameters?: Parameters<Paths.DeletePagePropertyById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeletePagePropertyById.Responses.$204>
  }
  ['/pages/{id}/versions']: {
    /**
     * getPageVersions - Get page versions
     * 
     * Returns the versions of specific page.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageVersions.PathParameters & Paths.GetPageVersions.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageVersions.Responses.$200>
  }
  ['/whiteboards']: {
    /**
     * createWhiteboard - Create whiteboard
     * 
     * Creates a whiteboard in the space.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the corresponding space. Permission to create a whiteboard in the space.
     */
    'post'(
      parameters?: Parameters<Paths.CreateWhiteboard.QueryParameters> | null,
      data?: Paths.CreateWhiteboard.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateWhiteboard.Responses.$200>
  }
  ['/whiteboards/{id}']: {
    /**
     * getWhiteboardById - Get whiteboard by id
     * 
     * Returns a specific whiteboard.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the whiteboard and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetWhiteboardById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWhiteboardById.Responses.$200>
    /**
     * deleteWhiteboard - Delete whiteboard
     * 
     * Delete a whiteboard by id.
     * 
     * Deleting a whiteboard moves the whiteboard to the trash, where it can be restored later
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the whiteboard and its corresponding space.
     * Permission to delete whiteboards in the space.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteWhiteboard.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/whiteboards/{id}/properties']: {
    /**
     * getWhiteboardContentProperties - Get content properties for whiteboard
     * 
     * Retrieves Content Properties tied to a specified whiteboard.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the whiteboard.
     */
    'get'(
      parameters?: Parameters<Paths.GetWhiteboardContentProperties.PathParameters & Paths.GetWhiteboardContentProperties.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWhiteboardContentProperties.Responses.$200>
    /**
     * createWhiteboardProperty - Create content property for whiteboard
     * 
     * Creates a new content property for a whiteboard.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the whiteboard.
     */
    'post'(
      parameters?: Parameters<Paths.CreateWhiteboardProperty.PathParameters> | null,
      data?: Paths.CreateWhiteboardProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateWhiteboardProperty.Responses.$200>
  }
  ['/whiteboards/{whiteboard-id}/properties/{property-id}']: {
    /**
     * getWhiteboardContentPropertiesById - Get content property for whiteboard by id
     * 
     * Retrieves a specific Content Property by ID that is attached to a specified whiteboard.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the whiteboard.
     */
    'get'(
      parameters?: Parameters<Paths.GetWhiteboardContentPropertiesById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWhiteboardContentPropertiesById.Responses.$200>
    /**
     * updateWhiteboardPropertyById - Update content property for whiteboard by id
     * 
     * Update a content property for a whiteboard by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the whiteboard.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateWhiteboardPropertyById.PathParameters> | null,
      data?: Paths.UpdateWhiteboardPropertyById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateWhiteboardPropertyById.Responses.$200>
    /**
     * deleteWhiteboardPropertyById - Delete content property for whiteboard by id
     * 
     * Deletes a content property for a whiteboard by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the whiteboard.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteWhiteboardPropertyById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeleteWhiteboardPropertyById.Responses.$204>
  }
  ['/whiteboards/{id}/operations']: {
    /**
     * getWhiteboardOperations - Get permitted operations for a whiteboard
     * 
     * Returns the permitted operations on specific whiteboard.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the whiteboard and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetWhiteboardOperations.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWhiteboardOperations.Responses.$200>
  }
  ['/whiteboards/{id}/ancestors']: {
    /**
     * getWhiteboardAncestors - Get all ancestors of the whiteboard
     * 
     * Returns all ancestors for a given whiteboard by ID in top-to-bottom order (that is, the highest ancestor is the first
     * item in the response payload). The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available by calling this endpoint with the ID of first ancestor in the response payload.
     * 
     * This endpoint returns minimal information about each ancestor. To fetch more details, use a related endpoint, such
     * as [Get page by id](https://developer.atlassian.com/cloud/confluence/rest/v2/api-group-page/#api-pages-id-get).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * Permission to view the whiteboard and its corresponding space
     */
    'get'(
      parameters?: Parameters<Paths.GetWhiteboardAncestors.PathParameters & Paths.GetWhiteboardAncestors.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWhiteboardAncestors.Responses.$200>
  }
  ['/pages/{page-id}/versions/{version-number}']: {
    /**
     * getPageVersionDetails - Get version details for page version
     * 
     * Retrieves version details for the specified page and version number.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the page.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageVersionDetails.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageVersionDetails.Responses.$200>
  }
  ['/custom-content/{custom-content-id}/versions']: {
    /**
     * getCustomContentVersions - Get custom content versions
     * 
     * Returns the versions of specific custom content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the custom content and its corresponding page and space.
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentVersions.PathParameters & Paths.GetCustomContentVersions.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentVersions.Responses.$200>
  }
  ['/custom-content/{custom-content-id}/versions/{version-number}']: {
    /**
     * getCustomContentVersionDetails - Get version details for custom content version
     * 
     * Retrieves version details for the specified custom content and version number.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the page.
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentVersionDetails.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentVersionDetails.Responses.$200>
  }
  ['/spaces']: {
    /**
     * getSpaces - Get spaces
     * 
     * Returns all spaces. The results will be sorted by id ascending. The number of results is limited by the `limit` parameter and
     * additional results (if available) will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * Only spaces that the user has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaces.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaces.Responses.$200>
  }
  ['/spaces/{id}']: {
    /**
     * getSpaceById - Get space by id
     * 
     * Returns a specific space.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaceById.PathParameters & Paths.GetSpaceById.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaceById.Responses.$200>
  }
  ['/spaces/{id}/blogposts']: {
    /**
     * getBlogPostsInSpace - Get blog posts in space
     * 
     * Returns all blog posts in a space. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission) and view the space.
     * Only blog posts that the user has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogPostsInSpace.PathParameters & Paths.GetBlogPostsInSpace.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogPostsInSpace.Responses.$200>
  }
  ['/spaces/{id}/labels']: {
    /**
     * getSpaceLabels - Get labels for space
     * 
     * Returns the labels of specific space. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaceLabels.PathParameters & Paths.GetSpaceLabels.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaceLabels.Responses.$200>
  }
  ['/spaces/{id}/content/labels']: {
    /**
     * getSpaceContentLabels - Get labels for space content
     * 
     * Returns the labels of space content (pages, blogposts etc). The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaceContentLabels.PathParameters & Paths.GetSpaceContentLabels.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaceContentLabels.Responses.$200>
  }
  ['/spaces/{id}/custom-content']: {
    /**
     * getCustomContentByTypeInSpace - Get custom content by type in space
     * 
     * Returns all custom content for a given type within a given space. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the custom content and the corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetCustomContentByTypeInSpace.PathParameters & Paths.GetCustomContentByTypeInSpace.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentByTypeInSpace.Responses.$200>
  }
  ['/spaces/{id}/operations']: {
    /**
     * getSpaceOperations - Get permitted operations for space
     * 
     * Returns the permitted operations on specific space.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaceOperations.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaceOperations.Responses.$200>
  }
  ['/spaces/{id}/pages']: {
    /**
     * getPagesInSpace - Get pages in space
     * 
     * Returns all pages in a space. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission) and 'View' permission for the space.
     * Only pages that the user has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetPagesInSpace.PathParameters & Paths.GetPagesInSpace.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPagesInSpace.Responses.$200>
  }
  ['/spaces/{space-id}/properties']: {
    /**
     * getSpaceProperties - Get space properties in space
     * 
     * Returns all properties for the given space. Space properties are a key-value storage associated with a space.
     * The limit parameter specifies the maximum number of results returned in a single response. Use the `link` response header
     * to paginate through additional results.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission) and 'View' permission for the space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaceProperties.PathParameters & Paths.GetSpaceProperties.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaceProperties.Responses.$200>
    /**
     * createSpaceProperty - Create space property in space
     * 
     * Creates a new space property.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission) and 'Admin' permission for the space.
     */
    'post'(
      parameters?: Parameters<Paths.CreateSpaceProperty.PathParameters> | null,
      data?: Paths.CreateSpaceProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateSpaceProperty.Responses.$201>
  }
  ['/spaces/{space-id}/properties/{property-id}']: {
    /**
     * getSpacePropertyById - Get space property by id
     * 
     * Retrieve a space property by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission) and 'View' permission for the space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpacePropertyById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpacePropertyById.Responses.$200>
    /**
     * updateSpacePropertyById - Update space property by id
     * 
     * Update a space property by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission) and 'Admin' permission for the space.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateSpacePropertyById.PathParameters> | null,
      data?: Paths.UpdateSpacePropertyById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateSpacePropertyById.Responses.$200>
    /**
     * deleteSpacePropertyById - Delete space property by id
     * 
     * Deletes a space property by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission) and 'Admin' permission for the space.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteSpacePropertyById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeleteSpacePropertyById.Responses.$204>
  }
  ['/spaces/{id}/permissions']: {
    /**
     * getSpacePermissions - Get space permissions
     * 
     * Returns space permissions for a specific space.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpacePermissions.PathParameters & Paths.GetSpacePermissions.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpacePermissions.Responses.$200>
  }
  ['/pages/{id}/footer-comments']: {
    /**
     * getPageFooterComments - Get footer comments for page
     * 
     * Returns the root footer comments of specific page. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageFooterComments.PathParameters & Paths.GetPageFooterComments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageFooterComments.Responses.$200>
  }
  ['/pages/{id}/inline-comments']: {
    /**
     * getPageInlineComments - Get inline comments for page
     * 
     * Returns the root inline comments of specific page. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetPageInlineComments.PathParameters & Paths.GetPageInlineComments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageInlineComments.Responses.$200>
  }
  ['/blogposts/{id}/footer-comments']: {
    /**
     * getBlogPostFooterComments - Get footer comments for blog post
     * 
     * Returns the root footer comments of specific blog post. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogPostFooterComments.PathParameters & Paths.GetBlogPostFooterComments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogPostFooterComments.Responses.$200>
  }
  ['/blogposts/{id}/inline-comments']: {
    /**
     * getBlogPostInlineComments - Get inline comments for blog post
     * 
     * Returns the root inline comments of specific blog post. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlogPostInlineComments.PathParameters & Paths.GetBlogPostInlineComments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlogPostInlineComments.Responses.$200>
  }
  ['/footer-comments']: {
    /**
     * getFooterComments - Get footer comments
     * 
     * Returns all footer comments. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the container and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetFooterComments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetFooterComments.Responses.$200>
    /**
     * createFooterComment - Create footer comment
     * 
     * Create a footer comment.
     * 
     * The footer comment can be made against several locations: 
     * - at the top level (specifying pageId or blogPostId in the request body)
     * - as a reply (specifying parentCommentId in the request body)
     * - against an attachment (note: this is different than the comments added via the attachment properties page on the UI, which are referred to as version comments)
     * - against a custom content
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blogpost and its corresponding space. Permission to create comments in the space.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreateFooterComment.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateFooterComment.Responses.$201>
  }
  ['/footer-comments/{comment-id}']: {
    /**
     * getFooterCommentById - Get footer comment by id
     * 
     * Retrieves a footer comment by id
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the container and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetFooterCommentById.PathParameters & Paths.GetFooterCommentById.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetFooterCommentById.Responses.$200>
    /**
     * updateFooterComment - Update footer comment
     * 
     * Update a footer comment. This can be used to update the body text of a comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blogpost and its corresponding space. Permission to create comments in the space.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateFooterComment.PathParameters> | null,
      data?: Paths.UpdateFooterComment.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateFooterComment.Responses.$200>
    /**
     * deleteFooterComment - Delete footer comment
     * 
     * Deletes a footer comment. This is a permanent deletion and cannot be reverted.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blogpost and its corresponding space. Permission to delete comments in the space.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteFooterComment.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeleteFooterComment.Responses.$204>
  }
  ['/footer-comments/{id}/children']: {
    /**
     * getFooterCommentChildren - Get children footer comments
     * 
     * Returns the children footer comments of specific comment. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetFooterCommentChildren.PathParameters & Paths.GetFooterCommentChildren.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetFooterCommentChildren.Responses.$200>
  }
  ['/footer-comments/{id}/likes/count']: {
    /**
     * getFooterLikeCount - Get like count for footer comment
     * 
     * Returns the count of likes of specific footer comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page/blogpost and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetFooterLikeCount.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetFooterLikeCount.Responses.$200>
  }
  ['/footer-comments/{id}/likes/users']: {
    /**
     * getFooterLikeUsers - Get account IDs of likes for footer comment
     * 
     * Returns the account IDs of likes of specific footer comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page/blogpost and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetFooterLikeUsers.PathParameters & Paths.GetFooterLikeUsers.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetFooterLikeUsers.Responses.$200>
  }
  ['/footer-comments/{id}/operations']: {
    /**
     * getFooterCommentOperations - Get permitted operations for footer comment
     * 
     * Returns the permitted operations on specific footer comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the parent content of the footer comment and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetFooterCommentOperations.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetFooterCommentOperations.Responses.$200>
  }
  ['/footer-comments/{id}/versions']: {
    /**
     * getFooterCommentVersions - Get footer comment versions
     * 
     * Retrieves the versions of the specified footer comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetFooterCommentVersions.PathParameters & Paths.GetFooterCommentVersions.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetFooterCommentVersions.Responses.$200>
  }
  ['/footer-comments/{id}/versions/{version-number}']: {
    /**
     * getFooterCommentVersionDetails - Get version details for footer comment version
     * 
     * Retrieves version details for the specified footer comment version.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetFooterCommentVersionDetails.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetFooterCommentVersionDetails.Responses.$200>
  }
  ['/inline-comments']: {
    /**
     * getInlineComments - Get inline comments
     * 
     * Returns all inline comments. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetInlineComments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetInlineComments.Responses.$200>
    /**
     * createInlineComment - Create inline comment
     * 
     * Create an inline comment. This can be at the top level (specifying pageId or blogPostId in the request body)
     * or as a reply (specifying parentCommentId in the request body). Note the inlineCommentProperties object in the
     * request body is used to select the text the inline comment should be tied to. This is what determines the text 
     * highlighting when viewing a page in Confluence.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blogpost and its corresponding space. Permission to create comments in the space.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreateInlineComment.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateInlineComment.Responses.$201>
  }
  ['/inline-comments/{comment-id}']: {
    /**
     * getInlineCommentById - Get inline comment by id
     * 
     * Retrieves an inline comment by id
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blogpost and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetInlineCommentById.PathParameters & Paths.GetInlineCommentById.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetInlineCommentById.Responses.$200>
    /**
     * updateInlineComment - Update inline comment
     * 
     * Update an inline comment. This can be used to update the body text of a comment and/or to resolve the comment
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blogpost and its corresponding space. Permission to create comments in the space.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateInlineComment.PathParameters> | null,
      data?: Paths.UpdateInlineComment.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateInlineComment.Responses.$200>
    /**
     * deleteInlineComment - Delete inline comment
     * 
     * Deletes an inline comment. This is a permanent deletion and cannot be reverted.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blogpost and its corresponding space. Permission to delete comments in the space.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteInlineComment.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeleteInlineComment.Responses.$204>
  }
  ['/inline-comments/{id}/children']: {
    /**
     * getInlineCommentChildren - Get children inline comments
     * 
     * Returns the children inline comments of specific comment. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetInlineCommentChildren.PathParameters & Paths.GetInlineCommentChildren.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetInlineCommentChildren.Responses.$200>
  }
  ['/inline-comments/{id}/likes/count']: {
    /**
     * getInlineLikeCount - Get like count for inline comment
     * 
     * Returns the count of likes of specific inline comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page/blogpost and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetInlineLikeCount.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetInlineLikeCount.Responses.$200>
  }
  ['/inline-comments/{id}/likes/users']: {
    /**
     * getInlineLikeUsers - Get account IDs of likes for inline comment
     * 
     * Returns the account IDs of likes of specific inline comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page/blogpost and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetInlineLikeUsers.PathParameters & Paths.GetInlineLikeUsers.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetInlineLikeUsers.Responses.$200>
  }
  ['/inline-comments/{id}/operations']: {
    /**
     * getInlineCommentOperations - Get permitted operations for inline comment
     * 
     * Returns the permitted operations on specific inline comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the parent content of the inline comment and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetInlineCommentOperations.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetInlineCommentOperations.Responses.$200>
  }
  ['/inline-comments/{id}/versions']: {
    /**
     * getInlineCommentVersions - Get inline comment versions
     * 
     * Retrieves the versions of the specified inline comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetInlineCommentVersions.PathParameters & Paths.GetInlineCommentVersions.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetInlineCommentVersions.Responses.$200>
  }
  ['/inline-comments/{id}/versions/{version-number}']: {
    /**
     * getInlineCommentVersionDetails - Get version details for inline comment version
     * 
     * Retrieves version details for the specified inline comment version.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content of the page or blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetInlineCommentVersionDetails.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetInlineCommentVersionDetails.Responses.$200>
  }
  ['/comments/{comment-id}/properties']: {
    /**
     * getCommentContentProperties - Get content properties for comment
     * 
     * Retrieves Content Properties attached to a specified comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the comment.
     */
    'get'(
      parameters?: Parameters<Paths.GetCommentContentProperties.PathParameters & Paths.GetCommentContentProperties.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCommentContentProperties.Responses.$200>
    /**
     * createCommentProperty - Create content property for comment
     * 
     * Creates a new content property for a comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the comment.
     */
    'post'(
      parameters?: Parameters<Paths.CreateCommentProperty.PathParameters> | null,
      data?: Paths.CreateCommentProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateCommentProperty.Responses.$200>
  }
  ['/comments/{comment-id}/properties/{property-id}']: {
    /**
     * getCommentContentPropertiesById - Get content property for comment by id
     * 
     * Retrieves a specific Content Property by ID that is attached to a specified comment.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the comment.
     */
    'get'(
      parameters?: Parameters<Paths.GetCommentContentPropertiesById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCommentContentPropertiesById.Responses.$200>
    /**
     * updateCommentPropertyById - Update content property for comment by id
     * 
     * Update a content property for a comment by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the comment.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateCommentPropertyById.PathParameters> | null,
      data?: Paths.UpdateCommentPropertyById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateCommentPropertyById.Responses.$200>
    /**
     * deleteCommentPropertyById - Delete content property for comment by id
     * 
     * Deletes a content property for a comment by its id. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the comment.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteCommentPropertyById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeleteCommentPropertyById.Responses.$204>
  }
  ['/tasks']: {
    /**
     * getTasks - Get tasks
     * 
     * Returns all tasks. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * Only tasks that the user has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetTasks.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetTasks.Responses.$200>
  }
  ['/tasks/{id}']: {
    /**
     * getTaskById - Get task by id
     * 
     * Returns a specific task. 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the containing page or blog post and its corresponding space.
     */
    'get'(
      parameters?: Parameters<Paths.GetTaskById.PathParameters & Paths.GetTaskById.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetTaskById.Responses.$200>
    /**
     * updateTask - Update task
     * 
     * Update a task by id.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the containing page or blog post and view its corresponding space.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateTask.PathParameters & Paths.UpdateTask.QueryParameters> | null,
      data?: Paths.UpdateTask.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateTask.Responses.$200>
  }
  ['/pages/{id}/children']: {
    /**
     * getChildPages - Get child pages
     * 
     * Returns all child pages for given page id. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * Only pages that the user has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetChildPages.PathParameters & Paths.GetChildPages.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetChildPages.Responses.$200>
  }
  ['/custom-content/{id}/children']: {
    /**
     * getChildCustomContent - Get child custom content
     * 
     * Returns all child custom content for given custom content id. The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * Only custom content that the user has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetChildCustomContent.PathParameters & Paths.GetChildCustomContent.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetChildCustomContent.Responses.$200>
  }
  ['/pages/{id}/ancestors']: {
    /**
     * getPageAncestors - Get all ancestors of page
     * 
     * Returns all ancestors for a given page by ID in top-to-bottom order (that is, the highest ancestor is the first
     * item in the response payload). The number of results is limited by the `limit` parameter and additional results (if available)
     * will be available by calling this endpoint with the ID of first ancestor in the response payload.
     * 
     * This endpoint returns minimal information about each ancestor. To fetch more details, use a related endpoint, such
     * as [Get page by id](https://developer.atlassian.com/cloud/confluence/rest/v2/api-group-page/#api-pages-id-get).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetPageAncestors.PathParameters & Paths.GetPageAncestors.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPageAncestors.Responses.$200>
  }
  ['/user/access/check-access-by-email']: {
    /**
     * checkAccessByEmail - Check site access for a list of emails
     * 
     * Returns the list of emails from the input list that do not have access to site.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CheckAccessByEmail.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CheckAccessByEmail.Responses.$200>
  }
  ['/user/access/invite-by-email']: {
    /**
     * inviteByEmail - Invite a list of emails to the site
     * 
     * Invite a list of emails to the site.
     * 
     * Ignores all invalid emails and no action is taken for the emails that already have access to the site.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.InviteByEmail.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/data-policies/metadata']: {
    /**
     * getDataPolicyMetadata - Get data policy metadata for the workspace (EAP)
     * 
     * Returns data policy metadata for the workspace.
     * 
     * **[Permissions](#permissions) required:**
     * Only apps can make this request.
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetDataPolicyMetadata.Responses.$200>
  }
  ['/data-policies/spaces']: {
    /**
     * getDataPolicySpaces - Get spaces with data policies (EAP)
     * 
     * Returns all spaces. The results will be sorted by id ascending. The number of results is limited by the `limit` parameter and
     * additional results (if available) will be available through the `next` URL present in the `Link` response header.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Only apps can make this request.
     * Permission to access the Confluence site ('Can use' global permission).
     * Only spaces that the app has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetDataPolicySpaces.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetDataPolicySpaces.Responses.$200>
  }
}

export type Client = OpenAPIClient<OperationMethods, PathsDictionary>
