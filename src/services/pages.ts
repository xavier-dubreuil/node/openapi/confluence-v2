import { ClientService } from '@openapi/container';
import { Client, Components } from '../openapi.d';

export class PagesService extends ClientService<Client> {
  public async createPage (
    spaceId: string,
    parentId: string,
    title: string,
    content: string,
    status: Components.RequestBodies.PageCreateRequest["status"] = 'current'
  ): Promise<Components.Schemas.PageSingle> {
    const { data: page } = await this.client.createPage({}, {
      spaceId,
      status,
      parentId,
      title,
      body: {
        representation: 'wiki',
        value: content
      }
    });
    return page;
  }

  public async getById (pageId: string): Promise<Components.Schemas.PageSingle | undefined> {
    const { data: page } = await this.client.getPageById({ id: parseInt(pageId) });
    return page;
  }

  public async getOnePageByTitle (spaceId: string, title: string): Promise<Components.Schemas.PageBulk | null> {
    const { data } = await this.client.getPages({ 'space-id': [parseInt(spaceId)], title });
    if (data.results === undefined) {
      return null;
    }
    const filter = data.results.filter(page => page.title === title);
    if (filter.length === 0) {
      return null;
    }
    return filter[0];
  }
}
