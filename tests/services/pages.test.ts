import { expect, it } from '@jest/globals';
import { PagesService } from '../../src/services/pages';
import mockAxios from 'jest-mock-axios';
import { Client } from '../../src';
import OpenAPIClientAxios from 'openapi-client-axios';
import { getDefinition } from '../../src/container';

describe('Service Pages', () => {
  it('CreatePage', async () => {
    const api = new OpenAPIClientAxios({ definition: await getDefinition() });
    const client = await api.init<Client>();
    const service = new PagesService(client);
    const promise = service.createPage('12345', '', 'Title', 'Content');
    expect(mockAxios.request).toHaveBeenCalledWith({
      method: 'post',
      url: '/pages',
      data: {
        spaceId: '12345',
        status: 'current',
        parentId: '',
        title: 'Title',
        body: {
          representation: 'wiki',
          value: 'Content',
        }
      },
      headers: {},
      params: {}
    });
    const response = { data: 'page-created' };
    mockAxios.mockResponse(response);
    const data = await promise;
    expect(data).toEqual('page-created');
  });
  it('getById', async () => {
    const api = new OpenAPIClientAxios({ definition: await getDefinition() });
    const client = await api.init<Client>();
    const service = new PagesService(client);
    const promise = service.getById('12345');
    expect(mockAxios.request).toHaveBeenCalledWith({
      method: 'get',
      url: '/pages/12345',
      data: undefined,
      headers: {},
      params: {}
    });
    const response = { data: 'the-page' };
    mockAxios.mockResponse(response);
    const data = await promise;
    expect(data).toEqual('the-page');
  });
  it('getOnePageByTitle OK', async () => {
    const api = new OpenAPIClientAxios({ definition: await getDefinition() });
    const client = await api.init<Client>();
    const service = new PagesService(client);
    const promise = service.getOnePageByTitle('12345', 'Title');
    expect(mockAxios.request).toHaveBeenCalledWith({
      method: 'get',
      url: '/pages',
      data: undefined,
      headers: {},
      params: {
        'space-id': [12345],
        'title': 'Title'
      }
    });
    const response = { data: { results: [{ id: 1, title: 'Title' }, { id: 2, title: 'Title 2' }] } };
    mockAxios.mockResponse(response);
    const data = await promise;
    expect(data).not.toBeNull()
    expect(data?.id).toEqual(1);
  });
  it('getOnePageByTitle Missing', async () => {
    const api = new OpenAPIClientAxios({ definition: await getDefinition() });
    const client = await api.init<Client>();
    const service = new PagesService(client);
    const promise = service.getOnePageByTitle('12345', 'Something');
    expect(mockAxios.request).toHaveBeenCalledWith({
      method: 'get',
      url: '/pages',
      data: undefined,
      headers: {},
      params: {
        'space-id': [12345],
        'title': 'Something'
      }
    });
    const response = { data: {} };
    mockAxios.mockResponse(response);
    const data = await promise;
    expect(data).toBeNull()
  });
  it('getOnePageByTitle Missing', async () => {
    const api = new OpenAPIClientAxios({ definition: await getDefinition() });
    const client = await api.init<Client>();
    const service = new PagesService(client);
    const promise = service.getOnePageByTitle('12345', 'Tit');
    expect(mockAxios.request).toHaveBeenCalledWith({
      method: 'get',
      url: '/pages',
      data: undefined,
      headers: {},
      params: {
        'space-id': [12345],
        'title': 'Tit'
      }
    });
    const response = { data: { results: [{ id: 1, title: 'Title' }, { id: 2, title: 'Title 2' }] } };
    mockAxios.mockResponse(response);
    const data = await promise;
    expect(data).toBeNull()
  });
});
